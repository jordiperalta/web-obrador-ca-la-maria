/**
 * Name of the consent use of cookies index. It has to match with the backend CookieConsentRepository constant
 * COOKIE_CONSENT_REPOSITORY_INDEX
 *
 * @type {string}
 */
const ACCEPT_COOKIE_CONSENT_INDEX = 'acceptCookieConsent';

(function () {

    window.addEventListener('load', (event) => {

        let cookies = new CookieManager();
        let consent = cookies.get(ACCEPT_COOKIE_CONSENT_INDEX);

        if (typeof consent == 'undefined') {

             showCookieBar();
        }

        let cookieAcceptSelector = document.querySelector("#accept-cookies");
        if (cookieAcceptSelector)
            cookieAcceptSelector.addEventListener('click', (event) => {

                let cookies = new CookieManager();
                cookies.set(ACCEPT_COOKIE_CONSENT_INDEX, 'true');
                hideCookieBar();
            })

        let cookieRefuseSelector = document.querySelector("#refuse-cookies");
        if (cookieRefuseSelector)
            cookieRefuseSelector.addEventListener('click', (event) => {

                let cookies = new CookieManager();
                cookies.set(ACCEPT_COOKIE_CONSENT_INDEX, 'false');
                hideCookieBar();
            })

        let cookieCloseSelector = document.querySelector("#close-cookies");
        if (cookieCloseSelector)
            cookieCloseSelector.addEventListener('click', (event) => {

                hideCookieBar();
            })
    });

    function hideCookieBar() {
        document.querySelector('#cookie-consent').classList.remove('navbar-cookie-show');
        document.querySelector('#cookie-consent').classList.add('navbar-cookie-hide');
    }

    function showCookieBar() {
        document.querySelector('#cookie-consent').classList.remove('navbar-cookie-hide');
        document.querySelector('#cookie-consent').classList.add('navbar-cookie-show');
    }
}());

class CookieManager {

    static defaultExpirationDays = 90;
    map = {};

    constructor()
    {
        this.mapCookies();
    }

    set(name, value, expirationDays = null)
    {
        let expirationDate = CookieManager.getExpirationDateUTCString(expirationDays)
        this.map[name] = value;
        document.cookie = name + "=" + value + ";expires=" + expirationDate + ";path=/;SameSite=strict";
        return this;
    }

    get(name)
    {
        return this.map[name];
    }

    delete(name)
    {
        if (!this.map[name])
            return;
        delete this.map[name]
        document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        return this;
    }

    all()
    {
        return this.map;
    }

    mapCookies()
    {
        let cookies = document.cookie.split('; ');
        let map = {}
        cookies.forEach(function(node){
            let index = node.split('=');
            map[index[0]] = index[1];
        });
        this.map = map;
        return this;
    }

    static getExpirationDateUTCString(expirationDays)
    {
        expirationDays = expirationDays | CookieManager.defaultExpirationDays;
        let d = new Date();
        d.setTime(d.getTime() + (expirationDays*24*60*60*1000));
        return d.toUTCString();
    }
}