$(function () {

    const radioShippingAll = document.querySelectorAll('input[name="del-option"]');
    const divSelectAll = document.querySelectorAll('div.form-group');
    const selectOrderAll = document.querySelectorAll('select[name="order-date"]');
    const inputInvoiceProvince = document.getElementById('inv-zone');
    const divShippingContainer = document.getElementById('shipping-info');
    const inputShippingFields = divShippingContainer.querySelectorAll('input, select');
    const inputPersonalZip = document.getElementById('personal-zip');
    const inputPersonalCity = document.getElementById('personal-city');
    const inputInvoiceZip = document.getElementById('inv-zip');
    const inputInvoiceCity = document.getElementById('inv-city');
    const inputInvoiceAddress = document.getElementById('inv-addr');
    const inputShippingZip = document.getElementById('del-zip');
    const inputShippingCity = document.getElementById('del-city');
    const inputShippingProvince = document.getElementById('del-zone');
    const inputShippingAddress = document.getElementById('del-addr');
    const inputUseInvoiceAddress = document.getElementById('useInvoiceAddress');
    const allShippingZoneOptions = divShippingContainer.querySelectorAll('option.select-not-empty-option');
    const radioDeliveryOption = document.getElementById('opt-ship');
    const radioDeliveryLabel = document.querySelector('label[for="opt-ship"]');

    let radioDeliveryLabelDefaultContent;

    const onWindowLoadCallback = () => {
        if (radioDeliveryOption)
            radioDeliveryLabelDefaultContent = radioDeliveryLabel.innerHTML;
        actionOnZipBlur();
    }

    const actionOnZipKeyUp = () => {

        let personalZip = inputPersonalZip.value;

        if (personalZip.length === 5){
            actionOnZipBlur();
        } else {
            if (radioDeliveryOption)
                radioDeliveryOption.disabled = true;
        }
    }

    const actionOnZipBlur = () => {

        let personalZip = inputPersonalZip.value;

        updateOrderZipFields(inputInvoiceZip, personalZip, false);
        inputInvoiceProvince.value = getProvinciaById(personalZip.substr(0,2));
        updateCity(inputPersonalZip, [inputPersonalCity, inputInvoiceCity, inputShippingCity], deliveryOptions);
        updateProvince(inputShippingZip, [inputShippingProvince]);
    }

    /**
     * On shipping option change update the select option delivery date list
     */
    radioShippingAll.forEach((element) => {
        try {
            element.addEventListener('change', () => {
                updateDeliveryDateList(element.value);
                updateOrderTotal(element.value);
            })
        } catch (e) {
            console.warn(e.message);
        }
    });

    /**
     * Window on load listener
     */
    window.addEventListener('load', onWindowLoadCallback)

    /**
     * Personal info postal code on blur listener
     */
    inputPersonalZip.addEventListener('blur', actionOnZipBlur)

    /**
     * Personal info postal code on blur listener
     */
    inputPersonalZip.addEventListener('keyup', actionOnZipKeyUp)

    /**
     * Invoice info postal code on keyup listener
     */
    inputInvoiceZip.addEventListener('keyup', () => {

        if (inputInvoiceZip.value.length === 5) {
            inputInvoiceProvince.value = getProvinciaById(inputInvoiceZip.value.substr(0,2));
        }
        updateCity(inputInvoiceZip, [inputInvoiceCity], disableUseInvoiceAddressCheck);
    })

    /**
     *
     */
    inputUseInvoiceAddress.addEventListener('change', () => {
        fillShippingAddressFields();
    })

    /**
     * @param { string } option
     */
    function updateOrderTotal(option) {

        const shippingPrice = orderSubtotal >= shippingFreeThreshold ? platformShippingPrice2 : platformShippingPrice;
        const delivery = option === 'ship' ? shippingPrice : 0;
        let orderTotal = orderSubtotal + delivery;
        let subTextDecoration = 'text-decoration: none;';

        if (orderSubtotal >= shippingFreeThreshold) {
            if (delivery)
                subTextDecoration = 'color: #27a627;';
        }

        document.getElementById('shipment-order-amount').innerHTML = delivery.toFixed(2).toString();
        document.getElementById('shipment-order-text').setAttribute('style', subTextDecoration);
        document.getElementById('total-order-amount').innerHTML = orderTotal.toFixed(2).toString();
    }

    /**
     * @param { object }json
     */
    function deliveryOptions (json)
    {
        if (radioDeliveryOption) {
            radioDeliveryLabel.innerHTML = json.valid ?
                radioDeliveryLabelDefaultContent :
                radioDeliveryLabelAlternativeContent + '<b> ' + inputPersonalZip.value + '</b> )</span>';
            radioDeliveryOption.disabled = !json.valid;
        }
        optionToUseInvoiceAddressForShipping(json.valid);
        updateDeliveryOptions(json.valid);
    }

    /**
     * @param { object }json
     */
    function disableUseInvoiceAddressCheck(json) {
        inputUseInvoiceAddress.disabled = !json.valid;
    }

    /**
     *
     * @param { object } origin
     * @param { object[] } target
     * @param { function } callback
     */
    function updateCity(origin, target, callback) {
        if (origin.value.length === 5)
            fetch(root + 'api/rest/postal-code/poblacion/' + origin.value)
                .then((response) => {return response.json()})
                .then((json) => {
                    target.forEach((elem) => {
                        elem.value = json.city ? json.city : '';
                    })
                    if (callback)
                        callback(json);
                })
                .catch((error) => {
                    console.warn(error); });
    }

    /**
     * @param { object } origin
     * @param { object[] } target
     */
    function updateProvince(origin, target) {
        console.log(typeof origin);
        if (origin.value.length === 5) {
            const code = origin.value.substr(0, 2);
            target.forEach((elem) => {
                elem.value = getProvinciaById(code);
            });
        }
    }

    /**
     * @param enable
     */
    function optionToUseInvoiceAddressForShipping(enable) {
        if (enable) {
            inputUseInvoiceAddress.checked = true;
            inputUseInvoiceAddress.disabled = false;
            fillShippingAddressFields();
            return;
        }
        inputUseInvoiceAddress.checked = false;
        inputUseInvoiceAddress.disabled = true;
        inputShippingFields.forEach((element) => {
            element.readOnly = false;
        });
    }

    /**
     * @param element
     * @param id
     */
    function updateOrderZipFields(element, id) {
        let zip = element.value;
        if (!zip || zip === element.previousZip) {
            element.value = id;
            element.previousZip = id;
        }
    }

    /**
     * @param enabled
     */
    function updateDeliveryOptions(enabled) {
        if (radioDeliveryOption) {
            radioDeliveryOption.checked = false;
        } else {
            enabled = false;
        }
        updateDeliveryDateList(enabled ? 'ship' : 'pick');
    }

    /**
     * @param id
     */
    function updateDeliveryDateList(id)
    {
        toggleShippingFields(id);

        divSelectAll.forEach((element) => {
            element.style.display = 'none';
        })
        selectOrderAll.forEach((element) => {
            element.disabled = true;
        })
        document.querySelector('div.form-group.select-'+id).style.display = 'block';
        document.querySelector('select#select-date-'+id).disabled = false;
    }

    /**
     * Show/Hide shipping input form fields
     *
     * @param id
     */
    function toggleShippingFields(id)
    {
        if (id === 'ship' && radioDeliveryOption && radioDeliveryOption.checked) {

            divShippingContainer.style.display = 'block';
            inputShippingFields.forEach((element) => {
                element.disabled = false;
            });
            fillShippingAddressFields();
            return;
        }
        divShippingContainer.style.display = 'none';
        inputShippingFields.forEach((element) => {
            element.disabled = true;
        });
    }

    /**
     * Actions when "Use invoice address" check button has changed
     */
    function fillShippingAddressFields()
    {
        if (inputUseInvoiceAddress.checked) {
            readOnlyShippingAddressFields();
            return;
        }
        enableShippingAddressFields();
    }

    /**
     * Puts the values from Invoice fields to SHipping fields and sets them as readOnly
     */
    function readOnlyShippingAddressFields() {

        inputShippingAddress.value = inputInvoiceAddress.value;
        inputShippingAddress.readOnly = true;

        inputShippingCity.value = inputInvoiceCity.value;
        inputShippingCity.readOnly = true;

        inputShippingZip.value = inputInvoiceZip.value;
        inputShippingProvince.value = getProvinciaById(inputShippingZip.value.substr(0,2));
    }

    /**
     * Sets shipping fields disable attribute as false and sets fields values by default
     */
    function enableShippingAddressFields() {

        allShippingZoneOptions.forEach((elem) => {
            elem.disabled = false;
        });

        inputShippingZip.value = inputPersonalZip.value;
        inputShippingCity.value = inputPersonalCity.value;
        inputShippingProvince.value = inputInvoiceProvince.value;
        inputShippingAddress.value = '';
        inputShippingAddress.readOnly = false;
        inputShippingZip.readOnly = true;
        inputShippingProvince.readOnly = true;
    }

    /**
     * Returns the "provincia" name if postal code prefix is found
     * @param id
     * @returns {string|null}
     */
    function getProvinciaById(id) {
        const list = JSON.parse(provincias);
        for (const key in list) {
            if (list.hasOwnProperty(key))
                if (list[key].id == id)
                    return list[key].name;
        }
        return null;
    }
});

$(function () {
    $('.postal-code-popover').popover({
        container: '.cp-popover'
    });
});
