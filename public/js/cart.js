let cart;

window.addEventListener('load', (event) => {

    cart = cartController();
    cart.update();
});

class CartView {

    /**
     * @type {string}
     */
    static selector = 'div#cart-list';

    /**
     * {CartModel}
     */
    model;

    /**
     * CartView constructor
     * @param {CartModel} model
     */
    constructor(model) {

        this.model = model;
    }

    /**
     * Update cart status
     * @param id
     */
    update(id) {

        /**
         * Sets product total to zero (symbol displayed '-') by default.
         * Afterwards this value will be updated in #feedbackCatalogProducts method.
         */
        this.feedbackProductAmount(id, '-');

        let cart = this.model.get()
        let length = cart.length;
        let actual = length > 0 ?
            {'items': length, 'status': 'filled'} :
            {'items': '..', 'status': 'empty'};

        this.feedbackCartTotal(actual.items)
            .feedbackCartIcon(actual.status)
            .feedbackCatalogProducts(cart);
    }

    render(selector)
    {
        document.querySelector(selector).innerHTML = '';

        const options = CartView.optionsTemplate(10);
        const template = CartView.cartItemTemplate();
        const summary = this.model.map();
        let cartLength = 0;

        for (let key in summary.items ) {
            if (summary.items.hasOwnProperty(key)) {
                let items = summary.items;
                let vars = {
                    'id': key,
                    'name': items[key].name,
                    'price': items[key].price.toFixed(2).toString() + ' €',
                    'image': items[key].image,
                    'options': options,
                }
                document.querySelector(selector).innerHTML += CartView.renderTemplate(template, vars);
                cartLength ++;
            }
        }

        this.feedbackCartOptions(cartLength);
        if (cartLength === 0)
            document.querySelector(selector).innerHTML = `<h4 class="mt-4 ml-2">${messages['empty_cart']}</h4>`;
    }

    feedbackCartOptions(total)
    {
        document.querySelector('input[name="cart"]').value = JSON.stringify(this.model.get());

        if(total > 0) {
            document.querySelector('#submit-cart').removeAttribute('disabled');
            return;
        }
        document.querySelector('#submit-cart').setAttribute('disabled', '');
    }

    /**
     * Update total cart items added
     * @param total
     * @returns {CartView}
     */
    feedbackCartTotal(total)
    {
        this.feedbackCartOptions(total)
        document.querySelectorAll('span.cart-items')
            .forEach((elem) => {
                elem.innerHTML = total });
        return this;
    }

    /**
     * Toggle cart symbol style
     * @param status
     * @returns {CartView}
     */
    feedbackCartIcon(status)
    {
        document.querySelectorAll('button.btn-cart')
            .forEach((elem) => {
                elem.setAttribute('data-cart', status )});
        return this;
    }

    /**
     * Calculates and updates the total added items of each product if bigger than zero
     * @returns {CartView}
     */
    feedbackCatalogProducts(cart)
    {
        cart.forEach((id) => {
            let count = 0;
            cart.forEach((check) => {
                if (check === id) count ++;
            });
            this.feedbackProductAmount(id, count);
        });
        return this;
    }

    /**
     * Update product total added items by Id
     * @param {string} id
     * @param {int|string} qty
     */
    feedbackProductAmount(id, qty)
    {
        if (typeof id !== 'string')
            return;

        let max = qty;
        document.querySelectorAll(`select.cart-item-${id}`)
            .forEach((elem) => {
                // in case amount("qty") is equal to 9 or minor the "max" value must be 10, otherwise is equal to "qty"
                max = qty > 9 ? qty : 10;

                // the value of the last option is reset to the value of "max"
                const last = elem.querySelector('option.last-option');
                last.value = max.toString();
                last.innerHTML = ' ' + max.toString();

                // in any case the value of the "select" element is "qty"
                elem.value = qty.toString();
            });

        if (typeof qty !== 'number') qty = 0;
        let summary = this.model.map();
        let subtotal = qty === 0 ? 0 : qty * summary.items[id].price;

        document.querySelectorAll(`.cart-item-subtotal-${id}`)
            .forEach((elem) => {
                elem.innerHTML = subtotal.toFixed(2).toString() + ' €';
            });

        document.querySelectorAll(`.cart-total-amount`)
            .forEach((elem) => {
                elem.innerHTML = summary.total.toFixed(2).toString() + ' €';
            });
    }

    /**
     *
     * @param {string} template
     * @param {object} map
     * @returns {string|null}
     */
    static renderTemplate(template, map)
    {
        if (typeof map !== 'object')
            return null;

        Object.entries(map).forEach(([key, value]) => {
            template = template.replace(new RegExp('{{ ' + key + ' }}', 'gi'), value.toString());
        });

        return template;
    }

    /**
     *
     * @param length
     * @returns {string|null}
     */
    static optionsTemplate(length)
    {
        if (typeof length !== 'number')
            return null;

        let options = '';
        let className = '';
        for (let i = 0; i <= length; i++) {
            if (i === length) {
                className = `class="last-option"`;
            }
            options += `
                <option ${className} value="${i}">`+ ( i > 0 ? i : '-' ) +`</option>`;
        }
        return options;
    }

    static cartItemTemplate()
    {
        return `
            <div class="row row-cart-list-item">
                <div class="col-12 col-sm-7 pl-0">
                    <div class="row pt-1 cart-list-item-link" data-toggle="modal" data-target="#id-{{ id }}" data-dismiss="modal" >
                        <img class="ml-3 mr-1" src="{{ image }}" alt="{{ id }}">
                        <div class="col pl-1 pt-1 cart-list-item-description">
                            <p class="text-left mb-0">{{ name }}</p>
                            <p class="text-left mb-0">
                                <span># ITEM &nbsp;</span>
                                {{ id }} 
                                <span class="cart-list-item-price"> &nbsp; {{ price }}</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-10 col-sm-5 ml-auto ml-sm-0">
                    <div class="row pt-1">
                        <div class="pr-2 ml-auto text-right cart-list-item-qty" style="width: 105px">
                            <label for="qty-{{ id }}">${messages.qty.toUpperCase()}. &nbsp;</label>
                            <select class="form-control form-control-xs cart-item-select cart-item-{{ id }}" 
                                    id="qty-{{ id }}" style="width: 50px" onchange="cart.set('{{ id }}', this)">
                                {{ options }}
                            </select>
                        </div>
                        <div class="pr-2 pt-1 cart-list-item-subtotal" style="width: 95px">
                            <p class="text-right mb-0 cart-item-subtotal-{{ id }}">&zwnj;</p>
                            <p class="text-right mb-0">
                                <a href="javascript:void(0)" onclick="cart.empty('{{ id }}')">
                                    ${messages.remove.toUpperCase()}
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>`
    }
}

function cartController() {

    const cartModel = new CartModel();
    const cartView = new CartView(cartModel);

    let updateCartView = (id) => {
        cartView.update(id);
    }

    let renderCartView = () => {
        cartView.render(CartView.selector);
    }

    let increaseProductById = (id, render = false) => {
        cartModel.increase(id);
        if (render)
            cartView.render(CartView.selector);
        cartView.update(id);
    }

    let decreaseProductById = (id, render = false) => {
        cartModel.decrease(id);
        if (render)
            cartView.render(CartView.selector);
        cartView.update(id);
    }

    let setProductQtyById = (id, elem, render = false) => {
        cartModel.addProducts(id, elem.value);
        if (render)
            cartView.render(CartView.selector);
        cartView.update(id);
    }

    let removeAllProductsById = (id) => {
        cartModel.removeProducts(id);
        cartView.render(CartView.selector);
        cartView.update(id);
    }

    let clearAllCartProducts = () => {
        cartModel.clear();
        cartView.update();
    }

    let getCartProducts = () => {
        return cartModel.get();
    }

    renderCartView();

    return ({
        get: getCartProducts,
        set: setProductQtyById,
        add: increaseProductById,
        remove: decreaseProductById,
        empty: removeAllProductsById,
        clear: clearAllCartProducts,
        update: updateCartView,
        render: renderCartView,
    });
}

class CartModel {

    cart;
    static cartLocalStorageKey = 'cart';

    /**
     * CartModel constructor
     */
    constructor()
    {
        this.retrieve();
    }

    /**
     * @returns {array}
     */
    get()
    {
        return this.cart;
    }

    /**
     * map cart by products
     * @returns {{}}
     */
    map()
    {
        let map = {'items':{},'total': 0};
        this.cart.forEach((id) => {
            if (typeof map.items[id] === 'undefined') {
                if (typeof catalog[id] !== 'undefined') {
                    map.items[id] = {
                        'qty': 0,
                        'name': catalog[id].name,
                        'price': parseFloat(catalog[id]['price_pack'] || catalog[id]['price_kilo'] || catalog[id]['price_unit']),
                        'description': catalog[id].description,
                        'image': assets + '/' + catalog[id].img.name,
                    };
                }
                if (typeof map.items[id] === 'undefined') {
                    this.decrease(id);
                }
                this.cart.forEach((elem) => {
                    if (elem === id) {
                        map.items[elem].qty ++;
                        map.total = map.total + parseFloat(map.items[elem].price)
                    }
                });
            }
        });
        return map;
    }

    /**
     * count qty of products in cart
     * @param {string} id
     * @returns {int}
     */
    count(id)
    {
        let count = 0;
        this.cart.forEach((elem) => {
            if (elem === id) count ++;
        });
        return count;
    }

    /**
     * @param {string} id
     * @returns {CartModel}
     */
    increase(id)
    {
        let price = catalog[id]['price_unit'] || catalog[id]['price_kilo'] || catalog[id]['price_pack'];
        if (isNaN(price))
            return this;

        this.cart.push(id);
        this.localUpdate();
        sessionStorage.setItem(CartModel.cartLocalStorageKey, this.cart);

        return this;
    }

    /**
     * @param {string} id
     * @returns {CartModel}
     */
    decrease(id)
    {
        const index = this.cart.indexOf(id);
        if (index > -1) {
            this.cart.splice(index, 1);
            this.localUpdate();
        }
        return this;
    }

    /**
     * @param {string} id
     * @param {int} qty
     * @returns {CartModel}
     */
    addProducts(id, qty)
    {
        this.removeProducts(id);
        for (let i = 0; i < qty; i ++) {
            this.increase(id);
        }

        return this;
    }

    /**
     * @param {string} id
     * @returns {CartModel}
     */
    removeProducts(id)
    {
        while (this.count(id) > 0) {
            this.decrease(id);
        }
        return this;
    }

    /**
     * @returns {CartModel}
     */
    clear()
    {
        this.cart = [];
        sessionStorage.removeItem(CartModel.cartLocalStorageKey);

        return this;
    }

    /**
     * @returns {CartModel}
     */
    localUpdate()
    {
        sessionStorage.setItem(CartModel.cartLocalStorageKey, this.cart);

        return this;
    }

    /**
     * @returns {CartModel}
     */
    retrieve()
    {
        let retrieve = sessionStorage.getItem(CartModel.cartLocalStorageKey);
        this.cart = retrieve ? retrieve.split(',') : [];
        this.decrease('undefined');

        return this;
    }
}
