let modal = false;

fadeOutPageLoader = () =>
{
    document.querySelectorAll('div#loading-screen').forEach(function (elem) {
        elem.classList.add('div-fade-out');
    });

    document.querySelectorAll('link#style-load').forEach(function (elem) {
        elem.setAttribute('href', root + 'css/style-defer.css');
    });

    setTimeout(function () {
        document.querySelectorAll('div#loading-screen').forEach(function (elem) {
            elem.parentNode.removeChild(elem);
        });
        // document.querySelectorAll('#critical-path-css').forEach(function (elem) {
        //     elem.parentNode.removeChild(elem);
        // });
    },2000);
}

$(function () {

    var lastScrollTop = 0;
    const minHideNavbarHeight = 900;
    const toggleNavbarStyleHeight = 270;

    let setNavbarStyle = (elem) =>
    {
        let scroll = $(window).scrollTop();

        if (scroll >= toggleNavbarStyleHeight) {
            elem.className = "navbar navbar-expand-lg navbar-dark fixed-top bg-dark"
        } else {
            elem.className = "navbar navbar-expand-lg navbar-light fixed-top bg-light navbar-transparent bg-transparent"
        }
    }

    let setDefaultCatalogViewStyle = () =>
    {
        if ($(window).width() < 992) {
            $('.catalog-list-container').attr('class', 'row catalogo-items catalog-list-group catalog-list-container');
        }
    }

    let showNavbar = (elem) =>
    {
        if (elem.hasClass('navbar-hide')) {
            elem.removeClass('navbar-hide')
        }
    }

    let hideNavbar = (elem) =>
    {
        if (!elem.hasClass('navbar-hide')) {
            elem.addClass('navbar-hide')
        }
    }

    let isScrollingDown = (event) =>
    {
        let scroll = $(this).scrollTop();
        if (scroll > lastScrollTop) {

            lastScrollTop = scroll;
            return true;
        } else {

            lastScrollTop = scroll;
            return false;
        }
    }

    let toggleNavbarPosition = (movingDown, array) =>
    {
        array.forEach( (elem) => {
            if (movingDown) {
                hideNavbar(elem);
            } else {
                showNavbar(elem);
            }
        });
    }

    let toggleCatalogDisplayButtons = () => {

        $("#catalog-view-switch").each(function () {

            if( isInViewport($("#catalog-items"), 'top')) {
                this.classList.remove('catalog-switcher-hide');
            } else {
                this.classList.add('catalog-switcher-hide');
            }
        });
    }

    let toggleIntroLogoAnimation = () => {

        $(".logo-brand-left,.logo-brand-right").each(function () {

            if( isInViewport(this, 'bottom', 200)) {

                this.classList.add('logo-brand-transition');
            } else {
                this.classList.remove('logo-brand-transition');
            }
        });
    }

    let enableCatalogListViewSwitcher = () => {

        $('#option-catalog-list').on("click", function () {
            $('.catalog-list-container').attr('class', 'row catalogo-items catalog-list-group catalog-list-container');
        });

        $('#option-catalog-icon').on("click", function () {
            $('.catalog-list-container').attr('class', 'row catalogo-items catalog-icon-group catalog-list-container');
        });

        toggleCatalogDisplayButtons();
    };

    let loadCatalog = (event) => {

        let loadBtnText = document.querySelector('#load-catalog-btn');
        if (loadBtnText)
            loadBtnText.innerHTML = `
            <div class="ball-pulse js-only">
                <div></div>
                <div></div>
                <div></div>
            </div>`;

        (event || Window.Event).preventDefault();

        fetch(root + 'api/catalog')
            .then((response) => response.text())
            .then((html) => {
                document.getElementById('catalog-items').innerHTML = html;
                document.getElementById('nav-catalog-dropdown').innerHTML = navCatalogDropdownTemplate();
                enableCatalogListViewSwitcher();
                setDefaultCatalogViewStyle();
            })
            .catch((error) => {
                console.warn(error); });

        loadCatalogModal();
    }

    /**
     * toggle main menu navbar style and hide/show depending on scroll direction and position
     */
    $(window).scroll(function (event) {

        /**
         * Toggle navbar style depending on scroll position
         */
        setNavbarStyle(document.querySelector("#main-navbar"));

        toggleNavbarPosition(isScrollingDown(event), [$("#main-navbar"),$("#catalog-view-switch")]);
        toggleCatalogDisplayButtons();
        toggleIntroLogoAnimation();

        /**
         * Section image background Parallax effect
         */
        $(".row.catalogo-article div:nth-child(1)").each(function() {
            let b = $(window).scrollTop();
            let d = $(window).height();
            let a = $(this).offset().top;
            let c = $(this).height();
            let e = (a - b + c) / (d + c);
            let f = (e < 0 ? 0 : e > 1 ? 1 : e) * 100;
            $(this).css('background-position', 'center ' + f + '%');
        });
    });

    /**
     * Mouseover effect on svg image
     */
    $('a#delivery-whatsapp')
        .on("mouseover", function () {
            document.querySelector('#c > feGaussianBlur')
                .setAttribute('stdDeviation', '20');
            document.querySelector('svg g > use#logo-shadow')
                .setAttribute('fill-opacity', '0.35');
            document.querySelector('svg > defs > linearGradient#a > stop:nth-child(1)')
                .setAttribute('stop-color', '#20A038');
            document.querySelector('svg > defs > linearGradient#a > stop:nth-child(2)')
                .setAttribute('stop-color', '#60C66A'); })
        .on("mouseout", function () {
            document.querySelector('#c > feGaussianBlur')
                .setAttribute('stdDeviation', '2');
            document.querySelector('svg g > use#logo-shadow')
                .setAttribute('fill-opacity', '0.25');
            document.querySelector('svg > defs > linearGradient#a > stop:nth-child(1)')
                .setAttribute('stop-color', '#20B038');
            document.querySelector('svg > defs > linearGradient#a > stop:nth-child(2)')
                .setAttribute('stop-color', '#60D66A'); })

    $(document).ready(function () {

        /**
         * set default styles of main menu navbar
         */
        setNavbarStyle(document.querySelector("#main-navbar"));
        enableCatalogListViewSwitcher();
        toggleIntroLogoAnimation();

        /**
         * hide main navbar after click on anchor link
         */
        $('#navbarMainMenu a.nav-anchor-link,footer a.nav-anchor-link').on('click', function () {
            let mainNavbar = $('#main-navbar');
            mainNavbar.toggleClass('active');
            $('#navbar-main-menu-toggler').trigger('click');
            setTimeout(function () {
                hideNavbar(mainNavbar);
            },1000);
            hideNavbar(mainNavbar);
        });
    })

    /**
     * main page loader fade-out on load
     */
    window.addEventListener('load', fadeOutPageLoader);

    /**
     * main page loader fade-out on click (if exists)
     */
    const loader = document.getElementById('loading-screen');

    if (loader)
        loader.addEventListener('click', fadeOutPageLoader);

    /**
     * load catalog with ajax request
     */
    const catalogButton = document.getElementById('load-catalog');

    if (catalogButton)
        catalogButton.addEventListener('click', loadCatalog);
});

function updatePostalCodeFeedback(origin) {
    console.log('checking')
    fetch(root + 'api/rest/postal-code/poblacion/' + origin.value)
        .then((response) => {return response.json()})
        .then((json) => {
            console.log(json);
            if (json.valid) {
                postalCodeHelp.innerHTML = postalCodeHelpSuccess;
                postalCodeHelp.className = 'text-success';
                return;
            }
            postalCodeHelp.innerHTML = postalCodeHelpWarning;
            postalCodeHelp.className = 'text-primary';
        })
        .catch((error) => {
            console.warn(error); });
}

const postalCodeInput = document.getElementById('inputPostalCode');
const postalCodeHelp = document.getElementById('helpPostalCode')
const actionOnPostalCodeUp = () => {

    if (postalCodeInput.value.length === 5){
        updatePostalCodeFeedback(postalCodeInput);
        document.getElementById('postal-code-modal-list').className = "div-display-block";
    } else {
        postalCodeHelp.innerText = postalCodeHelpInfo;
        postalCodeHelp.className = 'text-muted';
    }
}

if (postalCodeInput)
    postalCodeInput.addEventListener('keyup', actionOnPostalCodeUp);

function isInViewport(elem, type, reset = null) {

    let elementTop = $(elem).offset().top;
    let elementBottom = elementTop + $(elem).outerHeight() + reset; // reset: top-offset
    let viewportTop = $(window).scrollTop();
    let viewportBottom = viewportTop + $(window).height();
    let checkViewport = type === 'top' ? viewportTop : viewportBottom;

    return elementBottom > viewportTop && elementTop < checkViewport;
}

function navCatalogDropdownTemplate() {
    let options = navCatalogDropdownOptionsTemplate();
    return `
        <a class="nav-link dropdown-toggle" href="#" id="navbarCatalogoMenuLink" 
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="font-blanch">${catalogTitle}</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarCatalogoMenuLink">
            ${options}
        </div>`;
}

function navCatalogDropdownOptionsTemplate() {
    let options = '';
    for (let key in sections) {
        options += `
            <a class="dropdown-item nav-anchor-link" href="#section-${key}">
                <span class="font-blanch">
                    ${sections[key]['name_mobile']}
                </span>
            </a>`;
        }
    return options;
}

function loadCatalogModal() {
    if (modal)
        return;
    fetch(root + 'api/catalog-modal')
        .then((response) => response.text())
        .then((html) => {
            document.getElementById('catalog-modal-wrapper').innerHTML = html;
            cart.update();
        })
        .catch((error) => {
            console.warn(error); });
    modal = true;
}

function loadLegalContent(target, label) {
    fetch(root + 'api/legal/' + target, {method: 'POST'})
        .then((response) => response.text())
        .then((html) => {
            document.getElementById('legal-label').innerHTML = label;
            document.getElementById('legal-advice-content').innerHTML = html;
        })
        .catch((error) => {
            console.warn(error); });
}
