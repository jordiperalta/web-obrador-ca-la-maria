
let required = $('input,textarea,select').filter('[required]:visible');

required.focus(function () {
    let id = this.getAttribute('id');
    let element = getInputHelpElement(id);

    if (element) {
        element.classList.add('alert-hidden');
        element.innerHTML = '';
    }
})

required.blur(function () {
    getInputFeedback(this);
})

required.change(function () {
    checkFormValidation(this);
});

required.keyup(function () {
    checkFormValidation(this);
});

function getInputFeedback(input)
{
    let message = input.validationMessage;
    message = message && input.hasAttribute('pattern') ? input.getAttribute('title') : message;
    let id = input.getAttribute('id');
    inputFieldAlert(id, message);
}

function checkFormValidation(elem)
{
    let form = elem.closest('form');
    let recatpchaButton = form.querySelector('button.g-recaptcha')
    let nonValidatedButton = form.querySelector('button.non-validated-submit')

    if (form.checkValidity()) {

        recatpchaButton.classList.remove('submit-button-hidden');
        nonValidatedButton.classList.add('submit-button-hidden');
    } else {

        nonValidatedButton.classList.remove('submit-button-hidden');
        recatpchaButton.classList.add('submit-button-hidden');
    }
}

function onMailSubmitContact(token)
{
    console.log(token);
    document.getElementById('email-form').submit();
}

function inputFieldAlert(id, message)
{
    let element = getInputHelpElement(id);

    if (element) {
        element.innerHTML = message;

        if (message !== '')
            element.classList.remove('alert-hidden');
    }
}

function getInputHelpElement(id)
{
    return document.getElementById(id).parentNode.querySelector('div.input-field-alert');
}