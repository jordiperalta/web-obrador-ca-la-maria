---

# Project: Ca La María

Document updated on 22 Oct. 2020

### List of pending tasks:

 - Translation of common texts
 - Translation of catalog items and comments
 - ~~Cookie acceptance in frontend~~
 - ~~Add cookie acceptance logic in backend~~
 - Section "Distribución": 
   - Informatión
   - Images
   - Whatsapp logo and link
 - Design error pages templates
 - Configure smtp mailer for production
 
 - Configure language selector in navbar
 - Set by default catalog list view in mobile display