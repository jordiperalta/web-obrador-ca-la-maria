<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201227164343 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE site_info (id INT AUTO_INCREMENT NOT NULL, company_official VARCHAR(127) NOT NULL, company_commercial VARCHAR(63) NOT NULL, company_legal_short VARCHAR(31) NOT NULL, web_site VARCHAR(63) NOT NULL, web_domain VARCHAR(63) NOT NULL, cif VARCHAR(11) NOT NULL, registrar_data VARCHAR(255) NOT NULL, opening LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', address LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', phone_link VARCHAR(20) NOT NULL, phone_text VARCHAR(20) NOT NULL, whatsapp_link VARCHAR(20) NOT NULL, mobile_text VARCHAR(20) NOT NULL, official_email VARCHAR(63) NOT NULL, admin_email VARCHAR(63) NOT NULL, instagram_link VARCHAR(127) NOT NULL, facebook_link VARCHAR(127) DEFAULT NULL, shipping_price SMALLINT DEFAULT NULL, gateway_env VARCHAR(15) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE site_info');
    }
}
