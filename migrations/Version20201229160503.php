<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201229160503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE catalog (id INT AUTO_INCREMENT NOT NULL, ref VARCHAR(12) NOT NULL, section VARCHAR(15) NOT NULL, publish TINYINT(1) NOT NULL, name VARCHAR(63) NOT NULL, size VARCHAR(31) DEFAULT NULL, weight VARCHAR(31) DEFAULT NULL, units VARCHAR(31) DEFAULT NULL, piece VARCHAR(31) DEFAULT NULL, decorated VARCHAR(31) DEFAULT NULL, cold VARCHAR(63) DEFAULT NULL, image_flie LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', description VARCHAR(127) DEFAULT NULL, desciption_options LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', price_unit DOUBLE PRECISION DEFAULT NULL, price_kilo DOUBLE PRECISION DEFAULT NULL, price_pack DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE catalog');
    }
}
