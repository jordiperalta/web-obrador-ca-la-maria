<?php


namespace App\Model\Bridge\PostalCode;


use App\Entity\PostalCode;
use App\Model\Bridge\AbstractBridge;

class PostalCodeDbBridge extends AbstractBridge
{
    /**
     * @return array
     */
    public function towns(): array
    {
        $map = [];
        foreach ($this->doctrine->getRepository(PostalCode::class)->findAllOrderBy('loc') as $zone) {

            $map[] = $zone->map();
        }
        return $map;
    }
}