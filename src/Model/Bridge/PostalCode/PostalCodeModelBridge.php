<?php


namespace App\Model\Bridge\PostalCode;


class PostalCodeModelBridge
{
    /**
     * @var string[][]
     */
    public static $provincias = [
        ['id' => "01", "name" => "Araba"],
        ['id' => "02", "name" => "Albacete"],
        ['id' => "03", "name" => "Alicante"],
        ['id' => "04", "name" => "Almería"],
        ['id' => "33", "name" => "Asturias"],
        ['id' => "05", "name" => "Ávila"],
        ['id' => "06", "name" => "Badajoz"],
        ['id' => "07", "name" => "Illes Balears"],
        ['id' => "08", "name" => "Barcelona"],
        ['id' => "48", "name" => "Bizkaia"],
        ['id' => "09", "name" => "Burgos"],
        ['id' => "10", "name" => "Cáceres"],
        ['id' => "11", "name" => "Cádiz"],
        ['id' => "39", "name" => "Cantabria"],
        ['id' => "12", "name" => "Castellón"],
        ['id' => "51", "name" => "Ceuta"],
        ['id' => "13", "name" => "Ciudad Real"],
        ['id' => "14", "name" => "Córdoba"],
        ['id' => "15", "name" => "A Coruña"],
        ['id' => "16", "name" => "Cuenca"],
        ['id' => "17", "name" => "Girona"],
        ['id' => "18", "name" => "Granada"],
        ['id' => "19", "name" => "Guadalajara"],
        ['id' => "20", "name" => "Gipuzkoa"],
        ['id' => "21", "name" => "Huelva"],
        ['id' => "22", "name" => "Huesca"],
        ['id' => "23", "name" => "Jaen"],
        ['id' => "24", "name" => "Leon"],
        ['id' => "25", "name" => "Lleida"],
        ['id' => "27", "name" => "Lugo"],
        ['id' => "28", "name" => "Madrid"],
        ['id' => "29", "name" => "Málaga"],
        ['id' => "52", "name" => "Melilla"],
        ['id' => "30", "name" => "Murcia"],
        ['id' => "31", "name" => "Navarra"],
        ['id' => "32", "name" => "Ourense"],
        ['id' => "34", "name" => "Palencia"],
        ['id' => "35", "name" => "Las Palmas"],
        ['id' => "36", "name" => "Pontevedra"],
        ['id' => "26", "name" => "La Rioja"],
        ['id' => "37", "name" => "Salamanca"],
        ['id' => "40", "name" => "Segovia"],
        ['id' => "41", "name" => "Sevilla"],
        ['id' => "42", "name" => "Soria"],
        ['id' => "43", "name" => "Tarragona"],
        ['id' => "38", "name" => "Tenerife"],
        ['id' => "44", "name" => "Teruel"],
        ['id' => "45", "name" => "Toledo"],
        ['id' => "46", "name" => "Valencia"],
        ['id' => "47", "name" => "Valladolid"],
        ['id' => "49", "name" => "Zamora"],
        ['id' => "50", "name" => "Zaragoza"],
    ];

    public function towns()
    {
        return self::$towns;
    }

    private static $towns = [
        [
            'loc' => 'Badalona',
            'min' => 8911,
            'max' => 8918
        ],
        [
            'loc' => 'Barcelona',
            'min' => 8001,
            'max' => 8042
        ],
        [
            'loc' => 'L\'Hospitalet de Llobregat',
            'min' => 8901,
            'max' => 8908
        ],
        [
            'loc' => 'Montgat',
            'min' => 8390,
            'max' => 8390
        ],
        [
            'loc' => 'Sant Adrià de Besòs',
            'min' => 8930,
            'max' => 8930
        ],
        [
            'loc' => 'Santa Coloma de Gramenet',
            'min' => 8920,
            'max' => 8924
        ],
        [
            'loc' => 'Tiana',
            'min' => 8391,
            'max' => 8391
        ],
    ];
}