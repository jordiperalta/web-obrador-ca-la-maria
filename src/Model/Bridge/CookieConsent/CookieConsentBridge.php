<?php


namespace App\Model\Bridge\CookieConsent;


use Symfony\Component\HttpFoundation\Request;

class CookieConsentBridge
{
    /**
     * Name of the consent use of cookies index. It has to match with the frontend cookie.js constant
     * ACCEPT_COOKIE_CONSENT_INDEX
     *
     * @const string
     */
    const COOKIE_CONSENT_REPOSITORY_INDEX = 'acceptCookieConsent';

    /**
     * @param Request $request
     * @return string
     */
    public static function read(Request $request):? string
    {
        return $request->cookies->get(self::COOKIE_CONSENT_REPOSITORY_INDEX);
    }
}