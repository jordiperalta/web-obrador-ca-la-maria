<?php


namespace App\Model\Bridge\Catalog;


use App\Entity\CatalogSections;
use App\Model\Bridge\AbstractBridge;

class CatalogSectionsDbBridge extends AbstractBridge implements CatalogSectionsBridgeInterface
{
    /**
     * @return array
     */
    public function data(): array
    {
        $class = self::ENTITY_NAMESPACE . self::ENTITY_TAG;
        $map = [];
        foreach ($this->doctrine->getRepository(CatalogSections::class)->findAllOrderBySortSection() as $value) {
            $entity = $value->all()['section'];
            $key = array_key_first($entity);
            $map[$key] = $entity[$key];
        }
        return $map;
    }
}