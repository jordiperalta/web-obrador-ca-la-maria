<?php


namespace App\Model\Bridge\Catalog;


use App\Model\Bridge\BridgeInterface;

interface CatalogBridgeInterface extends BridgeInterface
{
    const ENTITY_TAG = 'Catalog';
}