<?php


namespace App\Model\Bridge\Catalog;


use App\Model\Bridge\BridgeInterface;

interface CatalogSectionsBridgeInterface extends BridgeInterface
{
    const ENTITY_TAG = 'CatalogSections';
}