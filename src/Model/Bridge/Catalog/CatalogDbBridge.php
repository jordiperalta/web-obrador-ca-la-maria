<?php


namespace App\Model\Bridge\Catalog;


use App\Entity\Catalog;
use App\Model\Bridge\AbstractBridge;

class CatalogDbBridge extends AbstractBridge implements CatalogBridgeInterface
{
    /**
     * @return array
     */
    public function data(): array
    {
        $map = [];
        foreach ($this->doctrine->getRepository(Catalog::class)->findAllOrderBy('ref', 'ASC') as $product) {

            $map[$product->getSection()][$product->getRef()] = $product->map();
        }
        return $map;
    }
}