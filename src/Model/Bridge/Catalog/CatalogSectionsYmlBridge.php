<?php


namespace App\Model\Bridge\Catalog;


use App\Model\Bridge\AbstractBridge;
use Symfony\Component\Yaml\Yaml;

class CatalogSectionsYmlBridge extends AbstractBridge implements CatalogSectionsBridgeInterface
{
    /**
     * @return array
     */
    public function data(): array
    {
        $filepath = $this->kernel->getProjectDir() . DIRECTORY_SEPARATOR .
            self::$data_directory . DIRECTORY_SEPARATOR . self::ENTITY_TAG . self::YAML_FILE_EXTENSION;

        return Yaml::parseFile($filepath);
    }
}