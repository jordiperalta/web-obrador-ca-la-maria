<?php


namespace App\Model\Bridge;



use App\Kernel;
use Symfony\Bridge\Doctrine\ManagerRegistry;

interface BridgeInterface
{
    /**
     * SiteInfoBridgeInterface constructor.
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     */
    public function __construct(Kernel $kernel, ManagerRegistry $doctrine);

    /**
     * @return array
     */
    public function data(): array;
}