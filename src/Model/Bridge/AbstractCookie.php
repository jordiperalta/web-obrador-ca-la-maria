<?php


namespace App\Model\Bridge;


abstract class AbstractCookie
{
    /**
     * @const int
     *
     * 90 days (90 x 24 x 60 x 60 seconds)
     */
    const DEFAULT_COOKIE_EXPIRATION_TIME = 90 * 24 * 60 * 60;
}