<?php


namespace App\Model\Bridge\Recaptcha;


use App\Model\Bridge\AbstractBridge;
use Symfony\Component\HttpFoundation\Request;

class RecaptchaBridge extends AbstractBridge implements RecaptchaRepositoryInterface
{
    /**
     * @const string
     */
    const RECAPTCHA_KEYS_FILE = 'recaptcha.yml';

    /**
     * @const string
     */
    const RECAPTCHA_SITE_KEY_INDEX = 'G_RECAPTCHA_SITE_KEY';

    /**
     * @const string
     */
    const RECAPTCHA_SECRET_KEY_INDEX = 'G_RECAPTCHA_SECRET_KEY';

    /**
     * @const string
     */
    const RECAPTCHA_SITE_FORM_INPUT_KEY = 'g-recaptcha-response';

    /**
     * @var array
     */
    public array $keys;

    /**
     * RecaptchaRepository constructor.
     * @param Request $request
     */
    public function __construct(?Request $request)
    {
        $this->keys = array(
            self::RECAPTCHA_SITE_KEY_INDEX => $request->server->get(self::RECAPTCHA_SITE_KEY_INDEX),
            self::RECAPTCHA_SECRET_KEY_INDEX => $request->server->get(self::RECAPTCHA_SECRET_KEY_INDEX)
        );
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getKey(string $key): ?string
    {
        if (!isset($this->keys[$key]))
            return null;

        return $this->keys[$key];
    }

    /**
     * @return array
     */
    public function getKeys(): array
    {
        return $this->keys;
    }

    /**
     * @return string
     */
    public function getSiteKey(): ?string
    {
        return $this->keys[self::RECAPTCHA_SITE_KEY_INDEX];
    }

    /**
     * @return string
     */
    public function getSecretKey(): ?string
    {
        return $this->keys[self::RECAPTCHA_SECRET_KEY_INDEX];
    }
}