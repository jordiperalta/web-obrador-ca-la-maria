<?php


namespace App\Model\Bridge\Recaptcha;


use Symfony\Component\HttpFoundation\Request;

interface RecaptchaRepositoryInterface
{
    /**
     * RecaptchaRepositoryInterface constructor.
     * @param Request $request
     */
    public function __construct(Request $request);

    /**
     * @param string $key
     * @return string|null
     */
    public function getKey(string $key): ?string;

    /**
     * @return array
     */
    public function getKeys(): array;

    /**
     * @return string
     */
    public function getSiteKey(): ?string;

    /**
     * @return string
     */
    public function getSecretKey(): ?string;
}