<?php


namespace App\Model\Bridge;


use App\Kernel;
use Doctrine\Persistence\ManagerRegistry;

abstract class AbstractBridge
{
    /**
     * @const string
     */
    const YAML_FILE_EXTENSION = '.yml';

    /**
     * @const string
     */
    const ENTITY_NAMESPACE = '\\App\\Entity\\';

    /**
     * @var Kernel
     */
    protected Kernel $kernel;

    /**
     * @var ManagerRegistry
     */
    protected ManagerRegistry $doctrine;

    /**
     * @var string
     */
    protected static string $data_directory = 'data';

    /**
     * AbstractFileBridge constructor.
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     */
    public function __construct(Kernel $kernel, ManagerRegistry $doctrine)
    {
        $this->kernel = $kernel;
        $this->doctrine = $doctrine;
    }
}