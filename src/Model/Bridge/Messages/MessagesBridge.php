<?php


namespace App\Model\Bridge\Messages;


use App\Kernel;
use App\Model\Bridge\AbstractBridge;
use Symfony\Component\Yaml\Yaml;

class MessagesBridge extends AbstractBridge implements MessagesBridgeInterface
{
    /**
     * @const string
     */
    const MESSAGES_REPOSITORY_FILE = 'messages.yml';

    /**
     * @param Kernel $kernel
     * @return array
     */
    public static function all(?Kernel $kernel): array
    {
        $filepath = $kernel->getProjectDir() .
            DIRECTORY_SEPARATOR . self::$data_directory . DIRECTORY_SEPARATOR . self::MESSAGES_REPOSITORY_FILE;

        return Yaml::parseFile($filepath);
    }
}