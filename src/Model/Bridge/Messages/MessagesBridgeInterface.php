<?php


namespace App\Model\Bridge\Messages;


use App\Kernel;

interface MessagesBridgeInterface
{
    /**
     * @param Kernel $kernel
     * @return array
     */
    public static function all(?Kernel $kernel): array;
}