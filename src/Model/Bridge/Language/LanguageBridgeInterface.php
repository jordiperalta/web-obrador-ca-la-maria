<?php


namespace App\Model\Bridge\Language;


interface LanguageBridgeInterface
{
    /**
     * @param $lang string
     * @return LanguageBridge
     */
    public function setDefault($lang): LanguageBridge;

    /**
     * @return string
     */
    public function getCurrent(): string;
}