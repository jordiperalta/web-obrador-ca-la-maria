<?php


namespace App\Model\Bridge\Language;


use App\Model\Bridge\AbstractCookie;
use App\Model\Bridge\CookieConsent\CookieConsentBridge;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LanguageBridge implements LanguageBridgeInterface
{
    /**
     * @const string
     */
    const LANGUAGE_REPOSITORY_INDEX = 'language';

    /**
     * @const string
     */
    const DEFAULT_LANGUAGE = 'es';

    /**
     * @var Request $request
     */
    private Request $request;

    /**
     * @var Response $response
     */
    private Response $response;

    /**
     * LanguageRepository constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->response = new Response();
    }

    /**
     * @param $lang string
     * @return LanguageBridge
     */
    public function setDefault($lang): LanguageBridge
    {
        if (CookieConsentBridge::read($this->request) === 'false')
            return $this;

        $cookie = Cookie::create(self::LANGUAGE_REPOSITORY_INDEX)
            ->withValue($lang)
            ->withExpires(time() + AbstractCookie::DEFAULT_COOKIE_EXPIRATION_TIME);

        $this->response->headers->setCookie($cookie);
        $this->response->sendHeaders();

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrent(): string
    {
        if (CookieConsentBridge::read($this->request) === 'false')
            return self::DEFAULT_LANGUAGE;

        return $this->request->cookies->get(self::LANGUAGE_REPOSITORY_INDEX) ?? self::DEFAULT_LANGUAGE;
    }
}