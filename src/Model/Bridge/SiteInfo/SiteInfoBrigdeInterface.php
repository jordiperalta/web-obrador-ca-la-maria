<?php


namespace App\Model\Bridge\SiteInfo;


use App\Model\Bridge\BridgeInterface;

interface SiteInfoBrigdeInterface extends BridgeInterface
{
    const ENTITY_TAG = 'SiteInfo';
}