<?php


namespace App\Model\Bridge\SiteInfo;


use App\Model\Bridge\AbstractBridge;

class SiteInfoDbBridge extends AbstractBridge implements SiteInfoBrigdeInterface
{
    /**
     * @return array
     */
    public function data(): array
    {
        $class = self::ENTITY_NAMESPACE . self::ENTITY_TAG;
        $data = $this->doctrine->getRepository($class)->findAll()[0]->all();
        $data['language'] = ['es' => 'es','ca' => 'ca','en' => 'en'];

        return $data;
    }
}