<?php


namespace App\Model\Services\Recaptcha;


use App\Model\Bridge\Recaptcha\RecaptchaBridge;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Request;

class RecaptchaValidationService
{
    /**
     * @const string
     */
    const GOOGLE_RECAPTCHA_VALIDATION_API_ENDPOINT_URL = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * @var string
     */
    private string $token;

    /**
     * @var string
     */
    private string $secretKey;

    /**
     * @var string
     */
    private string $ip;

    /**
     * RecaptchaValidationService constructor.
     * @param Request $request
     */
    private function __construct(Request $request)
    {
        $repository = new RecaptchaBridge($request);

        $this->secretKey = $repository->getSecretKey();
        $this->ip = (string)$request->server->get('REMOTE_ADDR');
        $this->token = (string)$request->request->get(RecaptchaBridge::RECAPTCHA_SITE_FORM_INPUT_KEY);
    }

    /**
     * @param Request $request
     * @return RecaptchaValidationService
     */
    public static function instance(Request $request):RecaptchaValidationService
    {
        return new self($request);
    }

    /**
     * @return string
     * @throws GuzzleException
     */
    public function verify(): ?string
    {
        $client = new Client(['base_uri' => self::GOOGLE_RECAPTCHA_VALIDATION_API_ENDPOINT_URL]);
        $form_params = [
            'secret' => $this->secretKey,
            'response' => $this->token,
            'remoteip' => $this->ip
        ];
        try {
            $response = $client->request('POST', self::GOOGLE_RECAPTCHA_VALIDATION_API_ENDPOINT_URL,
                array(
                    'form_params' => $form_params
                )
            );
            return $response->getBody()->getContents();

        } catch (GuzzleException $e) {

            var_dump($e);
        }
    }
}