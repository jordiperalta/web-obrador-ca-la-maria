<?php

namespace App\Entity;

use App\Repository\CatalogSectionsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatalogSectionsRepository::class)
 */
class CatalogSections
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="json")
     */
    private ?array $section = [];

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private ?int $sort_section;

    /**
     * @return array
     */
    public function all(): array
    {
        return get_object_vars($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSection(): ?array
    {
        return $this->section;
    }

    public function setSection(array $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getSortSection(): ?int
    {
        return $this->sort_section;
    }

    public function setSortSection(?int $sort_section): self
    {
        $this->sort_section = $sort_section;

        return $this;
    }
}
