<?php

namespace App\Entity;

use App\Repository\RedsysLogsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RedsysLogsRepository::class)
 */
class RedsysLogs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=127)
     */
    private ?string $host;

    /**
     * @ORM\Column(type="datetimetz_immutable")
     */
    private ?\DateTimeImmutable $date;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $size;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private ?array $headers = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $query;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $payload;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(?int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getHeaders(): ?array
    {
        return $this->headers;
    }

    public function setHeaders(?array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    public function getQuery(): ?string
    {
        return $this->query;
    }

    public function setQuery(?string $query): self
    {
        $this->query = $query;

        return $this;
    }

    public function getPayload(): ?string
    {
        return $this->payload;
    }

    public function setPayload(?string $payload): self
    {
        $this->payload = $payload;

        return $this;
    }
}
