<?php


namespace App\Entity;


trait EntityTrait
{
    /**
     * @return array
     */
    public function all(): array
    {
        return get_object_vars($this);
    }

    /**
     * @return array
     */
    public function map(): array
    {
        $map = [];

        foreach ($this->all() as $key => $value)
        {
            if (!empty($value))
                $map[$key] = $value;
        }
        return $map;
    }
}