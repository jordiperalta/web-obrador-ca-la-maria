<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=36)
     */
    private ?string $uuid;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $order_id;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $amount;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private ?string $status;

    /**
     * @ORM\Column(type="json")
     */
    private array $invoice_address = [];

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private ?array $shipping_address = [];

    /**
     * @ORM\Column(type="json")
     */
    private array $basket = [];

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private ?string $payment_datetime;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private ?string $delivery_date;

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     */
    private ?string $merchant_reference;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $authorization_number;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private ?string $redsys_response_code;

    /**
     * @ORM\Column(type="string", length=2)
     */
    private string $lang;

    /**
     * @ORM\Column(type="json")
     */
    private ?array $personal_info = [];

    /**
     * @ORM\Column(type="string", length=10)
     */
    private string $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getOrderId(): ?string
    {
        return $this->order_id;
    }

    public function setOrderId(string $order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getInvoiceAddress(): ?array
    {
        return $this->invoice_address;
    }

    public function setInvoiceAddress(array $invoice_address): self
    {
        $this->invoice_address = $invoice_address;

        return $this;
    }

    public function getShippingAddress(): ?array
    {
        return $this->shipping_address;
    }

    public function setShippingAddress(array $shipping_address): self
    {
        $this->shipping_address = $shipping_address;

        return $this;
    }

    public function getBasket(): ?array
    {
        return $this->basket;
    }

    public function setBasket(array $basket): self
    {
        $this->basket = $basket;

        return $this;
    }

    public function getPaymentDatetime(): ?string
    {
        return $this->payment_datetime;
    }

    public function setPaymentDatetime(?string $payment_datetime): self
    {
        $this->payment_datetime = $payment_datetime;

        return $this;
    }

    public function getDeliveryDate(): ?string
    {
        return $this->delivery_date;
    }

    public function setDeliveryDate(?string $delivery_date): self
    {
        $this->delivery_date = $delivery_date;

        return $this;
    }

    public function getMerchantReference(): ?string
    {
        return $this->merchant_reference;
    }

    public function setMerchantReference(?string $merchant_reference): self
    {
        $this->merchant_reference = $merchant_reference;

        return $this;
    }

    public function getAuthorizationNumber(): ?int
    {
        return $this->authorization_number;
    }

    public function setAuthorizationNumber(?int $authorization_number): self
    {
        $this->authorization_number = $authorization_number;

        return $this;
    }

    public function getRedsysResponseCode(): ?string
    {
        return $this->redsys_response_code;
    }

    public function setRedsysResponseCode(?string $redsys_response_code): self
    {
        $this->redsys_response_code = $redsys_response_code;

        return $this;
    }

    public function getLang(): ?string
    {
        return $this->lang;
    }

    public function setLang(string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    public function getPersonalInfo(): ?array
    {
        return $this->personal_info;
    }

    public function setPersonalInfo(array $personal_info): self
    {
        $this->personal_info = $personal_info;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
