<?php

namespace App\Entity;

use App\Repository\CatalogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatalogRepository::class)
 */
class Catalog
{
    use EntityTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=12)
     */
    private string $ref;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private string $section;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $publish;

    /**
     * @ORM\Column(type="string", length=63)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=31, nullable=true)
     */
    private ?string $size;

    /**
     * @ORM\Column(type="string", length=31, nullable=true)
     */
    private ?string $weight;

    /**
     * @ORM\Column(type="string", length=31, nullable=true)
     */
    private ?string $units;

    /**
     * @ORM\Column(type="string", length=31, nullable=true)
     */
    private ?string $piece;

    /**
     * @ORM\Column(type="string", length=31, nullable=true)
     */
    private ?string $decorated;

    /**
     * @ORM\Column(type="string", length=63, nullable=true)
     */
    private ?string $cold;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private ?array $image_file = [];

    /**
     * @ORM\Column(type="string", length=127, nullable=true)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private ?array $description_options = [];

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $price_unit;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $price_kilo;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $price_pack;

    /**
     * @ORM\Column(type="guid")
     */
    private string $uuid;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $iva;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getSection(): ?string
    {
        return $this->section;
    }

    public function setSection(string $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getPublish(): ?bool
    {
        return $this->publish;
    }

    public function setPublish(bool $publish): self
    {
        $this->publish = $publish;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSize(): ?string
    {
        return $this->size;
    }

    public function setSize(?string $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getWeight(): ?string
    {
        return $this->weight;
    }

    public function setWeight(?string $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getUnits(): ?string
    {
        return $this->units;
    }

    public function setUnits(?string $units): self
    {
        $this->units = $units;

        return $this;
    }

    public function getPiece(): ?string
    {
        return $this->piece;
    }

    public function setPiece(?string $piece): self
    {
        $this->piece = $piece;

        return $this;
    }

    public function getDecorated(): ?string
    {
        return $this->decorated;
    }

    public function setDecorated(?string $decorated): self
    {
        $this->decorated = $decorated;

        return $this;
    }

    public function getCold(): ?string
    {
        return $this->cold;
    }

    public function setCold(?string $cold): self
    {
        $this->cold = $cold;

        return $this;
    }

    public function getImageFile(): ?array
    {
        return $this->image_file;
    }

    public function setImageFile(?array $image_file): self
    {
        $this->image_file = $image_file;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescriptionOptions(): ?array
    {
        return $this->description_options;
    }

    public function setDescriptionOptions(?array $description_options): self
    {
        $this->description_options = $description_options;

        return $this;
    }

    public function getPriceUnit(): ?float
    {
        return $this->price_unit;
    }

    public function setPriceUnit(?float $price_unit): self
    {
        $this->price_unit = $price_unit;

        return $this;
    }

    public function getPriceKilo(): ?float
    {
        return $this->price_kilo;
    }

    public function setPriceKilo(?float $price_kilo): self
    {
        $this->price_kilo = $price_kilo;

        return $this;
    }

    public function getPricePack(): ?float
    {
        return $this->price_pack;
    }

    public function setPricePack(?float $price_pack): self
    {
        $this->price_pack = $price_pack;

        return $this;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getIva(): ?int
    {
        return $this->iva;
    }

    public function setIva(?int $iva): self
    {
        $this->iva = $iva;

        return $this;
    }
}
