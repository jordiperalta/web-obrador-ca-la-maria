<?php

namespace App\Entity;

use App\Repository\SiteInfoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SiteInfoRepository::class)
 */
class SiteInfo
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=127)
     */
    private ?string $company_official;

    /**
     * @ORM\Column(type="string", length=63)
     */
    private ?string $company_commercial;

    /**
     * @ORM\Column(type="string", length=31)
     */
    private ?string $company_legal_short;

    /**
     * @ORM\Column(type="string", length=63)
     */
    private ?string $web_site;

    /**
     * @ORM\Column(type="string", length=63)
     */
    private ?string $web_domain;

    /**
     * @ORM\Column(type="string", length=11)
     */
    private ?string $CIF;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $registrar_data;

    /**
     * @ORM\Column(type="json")
     */
    private ?array $opening = [];

    /**
     * @ORM\Column(type="json")
     */
    private ?array $address = [];

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $phone_link;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $phone_text;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $whatsapp_link;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $mobile_text;

    /**
     * @ORM\Column(type="string", length=63)
     */
    private ?string $official_email;

    /**
     * @ORM\Column(type="string", length=63)
     */
    private ?string $admin_email;

    /**
     * @ORM\Column(type="string", length=127)
     */
    private ?string $instagram_link;

    /**
     * @ORM\Column(type="string", length=127, nullable=true)
     */
    private ?string $facebook_link;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private ?int $shipping_price;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private ?string $gateway_env;

    /**
     * @ORM\Column(type="float")
     */
    private ?float $shipping_free_threshold;

    /**
     * @ORM\Column(type="integer")
     */
    private $shipping_price_2;

    /**
     * @return array
     */
    public function all(): array
    {
        return get_object_vars($this);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompanyOfficial(): ?string
    {
        return $this->company_official;
    }

    public function setCompanyOfficial(string $company_official): self
    {
        $this->company_official = $company_official;

        return $this;
    }

    public function getCompanyCommercial(): ?string
    {
        return $this->company_commercial;
    }

    public function setCompanyCommercial(string $company_commercial): self
    {
        $this->company_commercial = $company_commercial;

        return $this;
    }

    public function getCompanyLegalShort(): ?string
    {
        return $this->company_legal_short;
    }

    public function setCompanyLegalShort(string $company_legal_short): self
    {
        $this->company_legal_short = $company_legal_short;

        return $this;
    }

    public function getWebSite(): ?string
    {
        return $this->web_site;
    }

    public function setWebSite(string $web_site): self
    {
        $this->web_site = $web_site;

        return $this;
    }

    public function getWebDomain(): ?string
    {
        return $this->web_domain;
    }

    public function setWebDomain(string $web_domain): self
    {
        $this->web_domain = $web_domain;

        return $this;
    }

    public function getCIF(): ?string
    {
        return $this->CIF;
    }

    public function setCIF(string $CIF): self
    {
        $this->CIF = $CIF;

        return $this;
    }

    public function getRegistrarData(): ?string
    {
        return $this->registrar_data;
    }

    public function setRegistrarData(string $registrar_data): self
    {
        $this->registrar_data = $registrar_data;

        return $this;
    }

    public function getOpening(): ?array
    {
        return $this->opening;
    }

    public function setOpening(array $opening): self
    {
        $this->opening = $opening;

        return $this;
    }

    public function getAddress(): ?array
    {
        return $this->address;
    }

    public function setAddress(array $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhoneLink(): ?string
    {
        return $this->phone_link;
    }

    public function setPhoneLink(string $phone_link): self
    {
        $this->phone_link = $phone_link;

        return $this;
    }

    public function getPhoneText(): ?string
    {
        return $this->phone_text;
    }

    public function setPhoneText(string $phone_text): self
    {
        $this->phone_text = $phone_text;

        return $this;
    }

    public function getWhatsappLink(): ?string
    {
        return $this->whatsapp_link;
    }

    public function setWhatsappLink(string $whatsapp_link): self
    {
        $this->whatsapp_link = $whatsapp_link;

        return $this;
    }

    public function getMobileText(): ?string
    {
        return $this->mobile_text;
    }

    public function setMobileText(string $mobile_text): self
    {
        $this->mobile_text = $mobile_text;

        return $this;
    }

    public function getOfficialEmail(): ?string
    {
        return $this->official_email;
    }

    public function setOfficialEmail(string $official_email): self
    {
        $this->official_email = $official_email;

        return $this;
    }

    public function getAdminEmail(): ?string
    {
        return $this->admin_email;
    }

    public function setAdminEmail(string $admin_email): self
    {
        $this->admin_email = $admin_email;

        return $this;
    }

    public function getInstagramLink(): ?string
    {
        return $this->instagram_link;
    }

    public function setInstagramLink(string $instagram_link): self
    {
        $this->instagram_link = $instagram_link;

        return $this;
    }

    public function getFacebookLink(): ?string
    {
        return $this->facebook_link;
    }

    public function setFacebookLink(?string $facebook_link): self
    {
        $this->facebook_link = $facebook_link;

        return $this;
    }

    public function getShippingPrice(): ?int
    {
        return $this->shipping_price;
    }

    public function setShippingPrice(?int $shipping_price): self
    {
        $this->shipping_price = $shipping_price;

        return $this;
    }

    public function getGatewayEnv(): ?string
    {
        return $this->gateway_env;
    }

    public function setGatewayEnv(string $gateway_env): self
    {
        $this->gateway_env = $gateway_env;

        return $this;
    }

    public function getShippingFreeThreshold(): ?float
    {
        return $this->shipping_free_threshold;
    }

    public function setShippingFreeThreshold(float $shipping_free_threshold): self
    {
        $this->shipping_free_threshold = $shipping_free_threshold;

        return $this;
    }

    public function getShippingPrice2(): ?int
    {
        return $this->shipping_price_2;
    }

    public function setShippingPrice2(int $shipping_price_2): self
    {
        $this->shipping_price_2 = $shipping_price_2;

        return $this;
    }
}
