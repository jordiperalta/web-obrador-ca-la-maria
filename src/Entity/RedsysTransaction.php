<?php

namespace App\Entity;

use App\Repository\RedsysTransactionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RedsysTransactionRepository::class)
 */
class RedsysTransaction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private ?string $datetime;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private ?string $order_id;

    /**
     * @ORM\Column(type="string", length=24)
     */
    private ?string $merchant_reference;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $authorization_number;

    /**
     * @ORM\Column(type="string", length=4)
     */
    private ?string $response_code;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $amount;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetime(): ?string
    {
        return $this->datetime;
    }

    public function setDatetime(string $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getOrderId(): ?string
    {
        return $this->order_id;
    }

    public function setOrderId(string $order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }

    public function getMerchantReference(): ?string
    {
        return $this->merchant_reference;
    }

    public function setMerchantReference(string $merchant_reference): self
    {
        $this->merchant_reference = $merchant_reference;

        return $this;
    }

    public function getAuthorizationNumber(): ?int
    {
        return $this->authorization_number;
    }

    public function setAuthorizationNumber(?int $authorization_number): self
    {
        $this->authorization_number = $authorization_number;

        return $this;
    }

    public function getResponseCode(): ?string
    {
        return $this->response_code;
    }

    public function setResponseCode(string $response_code): self
    {
        $this->response_code = $response_code;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
