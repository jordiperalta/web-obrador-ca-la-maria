<?php


namespace App\Repository;


use App\Entity\Catalog;

trait RepositoryTrait
{
    /**
     * @return int|mixed|string
     */
    public function countAll()
    {
        return $this->createQueryBuilder('i')
            ->select('count(i.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $field
     * @param string $order
     * @return Catalog[] Returns an array of Catalog objects
     */
    public function findAllOrderBy(?string $field, ?string $order = 'ASC')
    {
        $order = strtoupper($order) == 'DESC' ? 'DESC' : 'ASC';
        return $this->createQueryBuilder('i')
            ->orderBy('i.' . $field, $order)
            ->getQuery()
            ->getResult();
    }
}