<?php

namespace App\Repository;

use App\Entity\RedsysLogs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RedsysLogs|null find($id, $lockMode = null, $lockVersion = null)
 * @method RedsysLogs|null findOneBy(array $criteria, array $orderBy = null)
 * @method RedsysLogs[]    findAll()
 * @method RedsysLogs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RedsysLogsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RedsysLogs::class);
    }

    // /**
    //  * @return RedsysLogs[] Returns an array of RedsysLogs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RedsysLogs
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
