<?php

namespace App\Repository;

use App\Entity\Catalog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Catalog|null find($id, $lockMode = null, $lockVersion = null)
 * @method Catalog|null findOneBy(array $criteria, array $orderBy = null)
 * @method Catalog[]    findAll()
 * @method Catalog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatalogRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    const MAX_RESULTS = 15;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Catalog::class);
    }

    /**
     * @param string $field
     * @param string $order
     * @return Catalog[] Returns an array of Catalog objects
     */
    public function findAllOrderBy(?string $field, ?string $order)
    {
        $order = strtoupper($order) == 'DESC' ? 'DESC' : 'ASC';
        return $this->createQueryBuilder('p')
            ->orderBy('p.' . $field, $order)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $field
     * @param string $order
     * @param int $page
     * @return Catalog[] Returns an array of Catalog objects
     */
    public function findAllOrderByLimit(?string $field, ?string $order, ?int $page = 0)
    {
        $order = strtoupper($order) == 'DESC' ? 'DESC' : 'ASC';
        return $this->createQueryBuilder('p')
            ->orderBy('p.' . $field, $order)
            ->setMaxResults(self::MAX_RESULTS)
            ->setFirstResult(self::MAX_RESULTS * $page)
            ->getQuery()
            ->getResult();
    }
}
