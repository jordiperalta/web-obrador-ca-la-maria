<?php

namespace App\Repository;

use App\Entity\CatalogSections;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CatalogSections|null find($id, $lockMode = null, $lockVersion = null)
 * @method CatalogSections|null findOneBy(array $criteria, array $orderBy = null)
 * @method CatalogSections[]    findAll()
 * @method CatalogSections[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatalogSectionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CatalogSections::class);
    }

    /**
     * @return CatalogSections[] Returns an array of Catalog objects
     */
    public function findAllOrderBySortSection()
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.sort_section', 'ASC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return CatalogSections[] Returns an array of CatalogSections objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CatalogSections
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
