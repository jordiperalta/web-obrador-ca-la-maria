<?php

namespace App\Repository;

use App\Entity\PostalCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PostalCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostalCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostalCode[]    findAll()
 * @method PostalCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostalCodeRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    const MAX_RESULTS = 15;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostalCode::class);
    }

    /**
     * @param string $field
     * @param string $order
     * @param int $page
     * @return PostalCode[] Returns an array of Catalog objects
     */
    public function findAllOrderByLimit(?string $field, ?string $order, ?int $page = 0)
    {
        $order = strtoupper($order) == 'DESC' ? 'DESC' : 'ASC';
        return $this->createQueryBuilder('p')
            ->orderBy('p.' . $field, $order)
            ->setMaxResults(self::MAX_RESULTS)
            ->setFirstResult(self::MAX_RESULTS * $page)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return PostalCode[] Returns an array of PostalCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PostalCode
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
