<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Driver\Exception as DriverException;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    const MAX_RESULTS = 15;

    /**
     * @var string[]
     */
    private static array $validSearchFields = ['uuid','order_id','personal_info'];

    /**
     * OrderRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @return Order[] Returns an array of Order objects
     */
    public function findCompletedOrders()
    {
        return $this->createQueryBuilder('o')
            ->where('o.status = :val OR o.redsys_response_code = :code OR o.authorization_number > :auth')
            ->setParameter('val', 'COMPLETED')
            ->setParameter('code', '0000')
            ->setParameter('auth', 999)
            ->orderBy('o.order_id', 'DESC')
            ->setMaxResults(self::MAX_RESULTS)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $page
     * @return Order[] Returns an array of Order objects
     */
    public function findCompletedOrdersByLimit(?int $page = 0)
    {
        return $this->createQueryBuilder('o')
            ->where('o.status = :val OR o.redsys_response_code = :code OR o.authorization_number > :auth')
            ->setParameter('val', 'COMPLETED')
            ->setParameter('code', '0000')
            ->setParameter('auth', 999)
            ->orderBy('o.order_id', 'DESC')
            ->setMaxResults(self::MAX_RESULTS)
            ->setFirstResult(self::MAX_RESULTS * $page)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $field
     * @param string $value
     * @param int|null $page
     * @return Order[] Returns an array of Order objects
     */
    public function searchOrderByField(string $field, string $value, ?int $page = 0)
    {
        $field = $this->isValidSearchField($field) ? $field : 'uuid';
        return $this->createQueryBuilder('o')
            ->where('o.status = :val')
            ->andWhere('o.' . $field . ' LIKE :field')
            ->setParameter('val', 'COMPLETED')
            ->setParameter('field', '%' . $value . '%')
            ->orderBy('o.order_id', 'DESC')
            ->setMaxResults(self::MAX_RESULTS)
            ->setFirstResult(self::MAX_RESULTS * $page)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countCompletedOrders()
    {
        return $this->createQueryBuilder('o')
            ->select('count(o.id)')
            ->where('o.status = :val OR o.redsys_response_code = :code OR o.authorization_number > :auth')
            ->setParameter('val', 'COMPLETED')
            ->setParameter('code', '0000')
            ->setParameter('auth', 999)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $field
     * @param string $value
     * @return int|mixed|string
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function countOrdersMatching(string $field, string $value)
    {
        $field = $this->isValidSearchField($field) ? $field : 'uuid';
        return $this->createQueryBuilder('o')
            ->select('count(o.id)')
            ->where('o.status = :status')
            ->andWhere('o.' . $field . ' LIKE :field')
            ->setParameter('status', 'COMPLETED')
            ->setParameter('field', '%' . $value . '%')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @return int|mixed|string
     * @throws Exception
     * @throws DriverException
     */
    public function countCompletedOrdersSql()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT count(*) FROM `order` 
            WHERE status = 'COMPLETED'";

        $result = $conn->prepare($sql);
        $result->execute();

        return $result->fetchOne();
    }

    /**
     * @param string $field
     * @return bool
     */
    private function isValidSearchField(string $field): bool
    {
        return in_array($field, self::$validSearchFields);
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
