<?php


namespace App\Service;


use App\Entity\LogsAdmin;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

class LoggerService
{
    private ObjectManager $manager;
    private LogsAdmin $adminLog;

    /**
     * @param ManagerRegistry $doctrine
     * @param Request $request
     * @return LoggerService
     */
    public function __construct(ManagerRegistry $doctrine, Request $request)
    {
        $this->manager = $doctrine->getManager();
        $this->adminLog = new LogsAdmin();

        $this->adminLog
            ->setData($request->request->all())
            ->setIpAddress($request->getClientIp());

        return $this;
    }

    /**
     * @param UserInterface $user
     * @param string $action
     * @param string $entity
     * @param string $uuid
     */
    public function log(UserInterface $user, string $action, string $entity, string $uuid = 'n/a'): void
    {
        $this->adminLog
            ->setAction($action)
            ->setUuid($uuid)
            ->setUser($user->getUsername())
            ->setEntity($entity)
            ->setDatetime(new \DateTimeImmutable());

        $this->manager->persist($this->adminLog);
        $this->manager->flush();
    }
}