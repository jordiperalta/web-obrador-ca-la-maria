<?php


namespace App\Controller\Services\Catalog;


use App\Controller\Route\FileAPI\ImageController;
use App\Kernel;
use App\Model\Bridge\Catalog\CatalogDbBridge;
use App\Model\Bridge\Catalog\CatalogSectionsDbBridge;
use App\Model\Bridge\Catalog\CatalogSectionsYmlBridge;
use App\Model\Bridge\Catalog\CatalogYmlBridge;
use Doctrine\Persistence\ManagerRegistry;

class CatalogService
{
    /**
     * @param string $lang
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     * @return array
     */
    public static function getItems(string $lang, Kernel $kernel, ManagerRegistry $doctrine): array
    {
        $dataManager = isset($doctrine) ?
            new CatalogDbBridge($kernel, $doctrine) :
            new CatalogYmlBridge($kernel, $doctrine);
        return self::mapArrayWithVerifiedFields(
            array_map(fn ($e) =>
            array_map(fn ($e) =>
            array_map(fn ($e) => $e[$lang] ?? $e['es'] ?? $e, $e), $e),
            $dataManager->data()), $kernel);
    }

    /**
     * @param string $lang
     * @param Kernel $kernel
     * @param ManagerRegistry|null $doctrine
     * @return array
     */
    public static function getSections(string $lang, Kernel $kernel, ManagerRegistry $doctrine = null): array
    {
        $dataManager = isset($doctrine) ?
            new CatalogSectionsDbBridge($kernel, $doctrine) :
            new CatalogSectionsYmlBridge($kernel, $doctrine);
        return
            array_map(fn ($e) =>
            array_map(fn ($e) => $e[$lang] ?? $e, $e),
            $dataManager->data());
    }

    /**
     * @param array $catalog
     * @param Kernel $kernel
     * @return array
     */
    private static function mapArrayWithVerifiedFields(array $catalog, Kernel $kernel)
    {
        $map = [];

        foreach ($catalog as $key1 => $item) {

            foreach ($item as $key2 => $category) {

                $map[$key1][$key2] = $category;

                foreach(['price_unit','price_kilo','price_pack'] as $price) {

                    if (isset($category[$price])){

                        $map[$key1][$key2][$price] = str_replace(',','.',$category[$price]);
                    }
                }
                $map[$key1][$key2]['img'] = ImageController::info(
                    ($category['image_file'][0] ?? 'null'), $kernel);
            }
        }
        clearstatcache();
        return $map;
    }
}