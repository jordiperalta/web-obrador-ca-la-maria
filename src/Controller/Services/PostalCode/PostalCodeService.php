<?php


namespace App\Controller\Services\PostalCode;


use App\Kernel;
use App\Model\Bridge\PostalCode\PostalCodeDbBridge;
use App\Model\Bridge\PostalCode\PostalCodeModelBridge;
use Doctrine\Persistence\ManagerRegistry;

class PostalCodeService
{
    /**
     * @param string $lang
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     * @return array
     */
    public static function getData(string $lang, Kernel $kernel, ManagerRegistry $doctrine = null): array
    {
        $dataManager = isset($doctrine) ?
            new PostalCodeDbBridge($kernel, $doctrine) :
            new PostalCodeModelBridge();
        $data = $dataManager->towns();
        return array_map(fn ($e) => $e[$lang] ?? $e, $data);
    }
}