<?php


namespace App\Controller\Services\Payment;


use App\Controller\Route\Payment\TransactionTrait;
use App\Controller\Services\Redsys\AbstractRedsys;
use App\Entity\Order;
use App\Entity\RedsysTransaction;
use App\Kernel;
use App\Model\API\Redsys\RedsysApiClass;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;

class ProcessService
{
    use TransactionTrait;

    /**
     * @var Request
     */
    private Request $request;

    /**
     * @var Kernel
     */
    private Kernel $kernel;

    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $doctrine;

    /**
     * @var ObjectManager
     */
    private ObjectManager $manager;

    /**
     * @var RedsysApiClass
     */
    private RedsysApiClass $redsys;

    /**
     * @var Order
     */
    private ?Order $order;

    /**
     * @var object
     */
    private object $data;

    /**
     * ProcessService constructor.
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param Kernel $kernel
     */
    private function __construct(Request $request, ManagerRegistry $doctrine, Kernel $kernel)
    {
        $this->request = $request;
        $this->doctrine = $doctrine;
        $this->manager = $this->doctrine->getManager();
        $this->kernel = $kernel;
    }

    /**
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @param Kernel $kernel
     * @return ProcessService
     */
    public static function instance(Request $request, ManagerRegistry $doctrine, Kernel $kernel): ProcessService
    {
        return new self($request, $doctrine, $kernel);
    }

    /**
     * @return object
     */
    public function getData(): object
    {
        return $this->data;
    }

    /**
     * @return string|null
     */
    public function verifyRequestErrors():? string
    {
        $this->redsys = new RedsysApiClass;

        try {
            $signatureOK = $this->request->getMethod() === 'POST' ?
                $this->validatePostReturnedSignature($this->request->request) :
                $this->validateGetReturnedSignature($this->request->query);
        } catch (Exception $e) {
            return 'Wrong formatted signature';
        }

        if (!$signatureOK)
            return 'Signature not valid';

        $this->order = $this->doctrine
            ->getRepository(Order::class)
            ->findOneBy(['order_id' => $this->data->Ds_Order]);

        if (!$this->order)
            return 'Order transaction not valid';

        return null;
    }

    public function isAcceptedPayment()
    {
        return self::redsysPaymentSuccessful($this->data->Ds_Response);
    }

    /**
     * @param MailerInterface $mailer
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     * @return Order|null
     */
    public function confirmationAction(MailerInterface $mailer, Kernel $kernel, ManagerRegistry $doctrine)
    {
        if ($this->order->getStatus() !== 'COMPLETED') {
            ConfirmationMailerService::instance(
                $this->order,
                $this->data,
                $mailer,
                $kernel,
                $doctrine)->send();
            $this->updateSuccessfulPaymentOrder();
        }

        return $this->order;
    }

    /**
     * @return bool
     */
    public function isTransactionAlreadyPersisted()
    {
        return !empty($this->doctrine
            ->getRepository(RedsysTransaction::class)
            ->findOneBy(['order_id' => $this->data->Ds_Order]));
    }

    /**
     * @return ProcessService
     */
    public function persistRedsysTransactionAttempt(): ProcessService
    {
        $type = $this->paymentGatewayOnProductionMode($this->kernel, $this->doctrine) ? 'REAL' : 'TEST';
        $transaction = new RedsysTransaction();
        $transaction
            ->setOrderId($this->data->Ds_Order)
            ->setDatetime($this->getTransactionDatetime())
            ->setAuthorizationNumber((int)$this->data->Ds_AuthorisationCode ?? null)
            ->setResponseCode($this->data->Ds_Response)
            ->setMerchantReference($this->data->Ds_MerchantCode)
            ->setAmount($this->data->Ds_Amount)
            ->setType($type);

        $this->manager->persist($transaction);
        $this->manager->flush();

        return $this;
    }

    /**
     * @return ProcessService
     */
    private function updateSuccessfulPaymentOrder()
    {
        $this->order
            ->setStatus('COMPLETED')
            ->setMerchantReference($this->data->Ds_MerchantCode)
            ->setAuthorizationNumber($this->data->Ds_AuthorisationCode)
            ->setRedsysResponseCode($this->data->Ds_Response)
            ->setPaymentDatetime($this->getTransactionDatetime());

        $this->manager->persist($this->order);
        $this->manager->flush();

        return $this;
    }

    /**
     * @return string
     */
    private function getTransactionDatetime(): string
    {
        return urldecode($this->data->Ds_Date) . ' ' . urldecode($this->data->Ds_Hour);
    }

    /**
     * @param InputBag $getVars
     * @return bool
     */
    private function validateGetReturnedSignature(InputBag $getVars)
    {
        return $this->verifySignature($getVars->all());
    }

    /**
     * @param ParameterBag $postVars
     * @return bool
     */
    private function validatePostReturnedSignature(ParameterBag $postVars)
    {
        return $this->verifySignature($postVars->all());
    }

    /**
     * @param array $vars
     * @return bool
     */
    private function verifySignature(array $vars)
    {
        $encoded = $vars['Ds_MerchantParameters'];
        $json = $this->redsys->decodeMerchantParameters($encoded);
        $this->data = json_decode($json);

        return $vars['Ds_Signature'] === $this->redsys->createMerchantSignatureNotif(
            $this->request->server->get(
                AbstractRedsys::getDsSignatureIndex(
                    $this->paymentGatewayOnProductionMode($this->kernel, $this->doctrine))),
            $encoded);
    }
}
