<?php


namespace App\Controller\Services\Payment;


use App\Controller\Route\Checkout\CheckoutTrait;
use App\Controller\Services\SiteInfo\SiteInfoService;
use App\Entity\Order;
use App\Kernel;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Translation\Translator;

class ConfirmationMailerService
{
    use checkoutTrait;

    /**
     * @var Order
     */
    private Order $order;

    /**
     * @var object
     */
    private object $data;

    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    /**
     * @var Kernel
     */
    private Kernel $kernel;

    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $doctrine;

    /**
     * @var DataCollectorTranslator
     */
    private DataCollectorTranslator $translator;

    /**
     * @var string|null
     */
    private ?string $locale;

    /**
     * ConfirmationMailerService constructor.
     * @param Order $order
     * @param object $data
     * @param MailerInterface $mailer
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     */
    private function __construct(Order $order, object $data, MailerInterface $mailer, Kernel $kernel, ManagerRegistry $doctrine)
    {
        $this->order = $order;
        $this->data = $data;
        $this->mailer = $mailer;
        $this->kernel = $kernel;
        $this->doctrine = $doctrine;
        $this->locale = $this->order->getLang();
        $this->translator = new DataCollectorTranslator(new Translator($this->locale));
        $this->translator->setLocale($this->locale);
    }

    /**
     * @param Order $order
     * @param object $data
     * @param MailerInterface $mailer
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     * @return ConfirmationMailerService
     */
    public static function instance(Order $order, object $data, MailerInterface $mailer, Kernel $kernel, ManagerRegistry $doctrine): ConfirmationMailerService
    {
        return new self($order, $data, $mailer, $kernel, $doctrine);
    }

    /**
     *
     */
    public function send()
    {
        $admin_email = SiteInfoService::getData($this->locale, $this->kernel, $this->doctrine)['admin_email'];
        try {
            $this->sendAdminMail($admin_email)->sendUserMail();
        } catch (TransportExceptionInterface $e) {}

        return $this;
    }

    /**
     * @param string $admin
     * @return ConfirmationMailerService
     * @throws TransportExceptionInterface
     */
    private function sendAdminMail(string $admin)
    {
        $info = $this->order->getPersonalInfo();
        $email = (new TemplatedEmail())
            ->from(new Address('no-reply@obrador-ca-la-maria.com', 'Obrador Ca La Maria'))
            ->replyTo(new Address($info['email'], $this->composeName($info)))
            ->to($admin)
            ->subject('Pedido confirmado web Ca La María: ' . $this->order->getUuid())
            ->htmlTemplate('emails/payment-msg-to-admin.html.twig')
            ->context($this->mailVars());
        $this->mailer->send($email);

        return $this;
    }

    /**
     * @return ConfirmationMailerService
     * @throws TransportExceptionInterface
     */
    private function sendUserMail()
    {
        $info = $this->order->getPersonalInfo();
        $email = (new TemplatedEmail())
            ->from(new Address('no-reply@obrador-ca-la-maria.com', 'Obrador Ca La Maria'))
            ->to(new Address($info['email'], $this->composeName($info)))
            ->subject($this->titleText() . $this->order->getUuid())
            ->htmlTemplate('emails/payment-msg-to-user.html.twig')
            ->context($this->mailVars());
        $this->mailer->send($email);

        return $this;
    }

    /**
     * @param array $info
     * @return string
     */
    private function composeName(array $info)
    {
        return ucfirst($info['fname']) . ' ' . ucfirst($info['lname']);
    }

    /**
     * @return string
     */
    private function titleText(): string
    {
        switch ($this->locale)
        {
            case 'en':
                return 'Obrador Ca La Maria - Payment confirmation: ';
            case 'ca':
                return 'Obrador Ca La Maria - Confirmació de pagament: ';
        }
        return 'Obrador Ca La Maria - Confirmación de pago: ';
    }

    private function mailVars()
    {
        return
        [
            'order' => [
                [
                    'key' => 'cantidad',
                    'value' => strval(
                        number_format(
                            ((int)$this->order->getAmount())/100,2,'.','')) . ' €',
                ],
                [
                    'key' => 'id',
                    'value' => $this->order->getUuid(),
                ],
                [
                    'key' => 'fecha y hora',
                    'value' => $this->order->getPaymentDatetime(),
                ],
                [
                    'key' => 'referencia de transacción',
                    'value' => $this->order->getOrderId(),
                ],
            ],
            'delivery_date' => self::getDeliveryDateMap(strtotime($this->order->getDeliveryDate())),
            'delivery_info' => empty($this->order->getShippingAddress()) ? 'recogida en tienda' : 'envío a domicilio',
            'personal' => $this->order->getPersonalInfo(),
            'basket' => $this->order->getBasket()['items'],
            'sAddr' => $this->order->getShippingAddress(),
            'test' => $this->order->getType(),
        ];
    }
}