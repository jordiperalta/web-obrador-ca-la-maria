<?php


namespace App\Controller\Services\Redsys;


use App\Model\API\Redsys\RedsysApiClass;
use Symfony\Component\HttpFoundation\Request;

class RedsysRedirectService extends AbstractRedsys
{
    const DS_MERCHANT_RETURN_URI = 'payment';

    /**
     * @var RedsysApiClass
     */
    private RedsysApiClass $redsys;

    /**
     * RedirectionService constructor.
     */
    public function __construct()
    {
        $this->redsys = new RedsysApiClass;
        $this->redsys
            ->setParameter(self::KEY_MERCHANT_CURRENCY, self::DS_MERCHANT_CURRENCY)
            ->setParameter(self::KEY_MERCHANT_MERCHANTCODE, self::DS_MERCHANT_MERCHANTCODE)
            ->setParameter(self::KEY_MERCHANT_TERMINAL, self::DS_MERCHANT_TERMINAL)
            ->setParameter(self::KEY_MERCHANT_TRANSACTIONTYPE, self::DS_MERCHANT_TRANSACTIONTYPE);
    }

    /**
     * @param string $homepage
     * @param Request $request
     * @return RedsysRedirectService
     */
    public function setFeedbackURLs(string $homepage, Request $request): RedsysRedirectService
    {
        $this->redsys
            ->setParameter(self::KEY_MERCHANT_MERCHANTURL, $this->getMerchantURL($homepage, $request))
            ->setParameter(self::KEY_MERCHANT_URLKO, $homepage. self::DS_MERCHANT_RETURN_URI)
            ->setParameter(self::KEY_MERCHANT_URLOK, $homepage. self::DS_MERCHANT_RETURN_URI);
        return $this;
    }

    /**
     * @param int $amount
     * @return RedsysRedirectService
     */
    public function setAmount(int $amount): RedsysRedirectService
    {
        $this->redsys->setParameter(self::KEY_MERCHANT_AMOUNT, strval($amount));
        return $this;
    }

    /**
     * @param string $order
     * @return RedsysRedirectService
     */
    public function setOrder(string $order): RedsysRedirectService
    {
        $this->redsys->setParameter(self::KEY_MERCHANT_ORDER, $order);
        return $this;
    }

    /**
     * @param string
     * @param Request $request
     * @return array
     */
    public function getSendOutputVars(string $gatewayProductionMode, Request $request): array
    {
        return array(
            'ds_paymentURL' => self::getPaymentURL($gatewayProductionMode),
            'ds_signatureVersion' => self::DS_SIGNATURE_VERSION,
            'ds_merchantParameters' => $this->redsys->createMerchantParameters(),
            'ds_signature' => $this->redsys->createMerchantSignature(
                $request->server->get(self::getDsSignatureIndex($gatewayProductionMode))),
//            'ds_secret_index' => self::getDsSignatureIndex($gatewayProductionMode),
//            'ds_secret_value' => $request->server->get(self::getDsSignatureIndex($gatewayProductionMode)),
        );
    }

    /**
     * @param bool $gatewayProductionMode
     * @return string
     */
    private static function getPaymentURL(bool $gatewayProductionMode): string
    {
        return $gatewayProductionMode ? self::DS_PRODUCTION_PAYMENT_URL : self::DS_SANDBOX_PAYMENT_URL ;
    }

    /**
     * Returns the EndpointURL where Redsýs will send the confirmation message with POST params, this URL should be
     * configured in .env and it does not depend on sandbox-production payment configuration, it can be valid for both.
     * If it's not configured in .env, the default value set in AbstractRedsys::DS_MERCHANT_MERCHANTURL will be returned
     *
     * @param string $homepage
     * @param Request $request
     * @return string
     */
    private function getMerchantURL(string $homepage, Request $request): string
    {
        $redsysMerchantURI = $request->server->get('REDSYS_MERCHANT_URI',null);
        return $redsysMerchantURI ? $homepage . $redsysMerchantURI : self::DS_MERCHANT_MERCHANTURL;
    }
}