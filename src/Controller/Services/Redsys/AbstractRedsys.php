<?php


namespace App\Controller\Services\Redsys;


abstract class AbstractRedsys
{
    /**
     * These KEYS are defined by REDSYS to send a POST request to load the payment page
     */
    const KEY_MERCHANT_AMOUNT = 'DS_MERCHANT_AMOUNT';
    const KEY_MERCHANT_CURRENCY = 'DS_MERCHANT_CURRENCY';
    const KEY_MERCHANT_MERCHANTCODE = 'DS_MERCHANT_MERCHANTCODE';
    const KEY_MERCHANT_MERCHANTURL = 'DS_MERCHANT_MERCHANTURL';
    const KEY_MERCHANT_ORDER = 'DS_MERCHANT_ORDER';
    const KEY_MERCHANT_TERMINAL = 'DS_MERCHANT_TERMINAL';
    const KEY_MERCHANT_TRANSACTIONTYPE = 'DS_MERCHANT_TRANSACTIONTYPE';
    const KEY_MERCHANT_URLKO = 'DS_MERCHANT_URLKO';
    const KEY_MERCHANT_URLOK = 'DS_MERCHANT_URLOK';

    const DS_MERCHANT_CURRENCY = '978';
    const DS_MERCHANT_MERCHANTCODE = '350833380';
    const DS_MERCHANT_MERCHANTURL = 'https://webhook.site/b0f2d0ca-ba20-43ff-b3e2-4cb9c6e76672';
    // check at: https://webhook.site/#!/b0f2d0ca-ba20-43ff-b3e2-4cb9c6e76672/f829d321-abce-49f5-af02-c243b06d2108/1
    // webhook.site configured on: 18-Dec-2020
    const DS_MERCHANT_TERMINAL = '001';
    const DS_MERCHANT_TRANSACTIONTYPE = '0';

    const DS_SANDBOX_PAYMENT_URL = 'https://sis-t.redsys.es:25443/sis/realizarPago';
    const DS_PRODUCTION_PAYMENT_URL = 'https://sis.redsys.es/sis/realizarPago';
    const DS_SANDBOX_PRIVATE_COMMERCE_KEY = 'sq7HjrUOBfKmC576ILgskD5srU870gJ7';
    const DS_SDBX_ENV_INDEX = 'DS_SDBX_PRIVATE_SECRET_SHA256';
    const DS_PROD_ENV_INDEX = 'DS_PROD_PRIVATE_SECRET_SHA256';
    const DS_SIGNATURE_VERSION = 'HMAC_SHA256_V1';

    /**
     * @param bool $gatewayProductionMode
     * @return string
     */
    public static final function getDsSignatureIndex(bool $gatewayProductionMode): string
    {
        return $gatewayProductionMode ? self::DS_PROD_ENV_INDEX : self::DS_SDBX_ENV_INDEX;
    }
}