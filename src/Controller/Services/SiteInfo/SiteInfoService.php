<?php


namespace App\Controller\Services\SiteInfo;


use App\Kernel;
use App\Model\Bridge\SiteInfo\SiteInfoDbBridge;
use App\Model\Bridge\SiteInfo\SiteInfoYmlBridge;
use Doctrine\Persistence\ManagerRegistry;

class SiteInfoService
{
    /**
     * @param string $lang
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     * @return array
     */
    public static function getData(string $lang, Kernel $kernel, ManagerRegistry $doctrine = null): array
    {
        $dataManager = isset($doctrine) ?
            new SiteInfoDbBridge($kernel, $doctrine) :
            new SiteInfoYmlBridge($kernel, $doctrine);
        $data = $dataManager->data();
        return array_map(fn ($e) => $e[$lang] ?? $e, $data);
    }
}