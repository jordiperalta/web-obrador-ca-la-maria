<?php


namespace App\Controller\Services\Logs;


use App\Entity\RedsysLogs;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class RedsysLogService
{

    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $doctrine;

    /**
     * @var ObjectManager
     */
    private ObjectManager $manager;

    /**
     * RedsysLogService constructor.
     * @param ManagerRegistry $doctrine
     */
    private function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
        $this->manager = $this->doctrine->getManager();
    }

    /**
     * @param ManagerRegistry $doctrine
     * @return RedsysLogService
     */
    public static function instance(ManagerRegistry $doctrine): RedsysLogService
    {
        return new self($doctrine);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function log(Request $request)
    {
        $webhook = new RedsysLogs();
        $webhook
            ->setHost($request->getClientIp())
            ->setSize($request->server->getHeaders()['CONTENT_LENGTH'] ?? null)
            ->setDate(new \DateTimeImmutable())
            ->setHeaders($request->server->getHeaders())
            ->setQuery(http_build_query($request->query->all()))
            ->setPayload(http_build_query($request->request->all()));

        $this->manager->persist($webhook);
        $this->manager->flush();

        return $this;
    }
}
