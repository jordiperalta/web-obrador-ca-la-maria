<?php


namespace App\Controller\Services\Recaptcha;


use App\Model\Services\Recaptcha\RecaptchaValidationService;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Request;

class RecaptchaManagerService
{
    /**
     * @var RecaptchaValidationService
     */
    public RecaptchaValidationService $recaptchaValidation;

    /**
     * RecaptchaController constructor.
     * @param Request $request
     */
    private function __construct (Request $request)
    {
        $this->recaptchaValidation = RecaptchaValidationService::instance($request);
    }

    /**
     * @param Request $request
     * @return RecaptchaManagerService
     */
    public static function instance(Request $request): RecaptchaManagerService
    {
        return new self($request);
    }

    /**
     * @return object
     * @throws GuzzleException
     */
    public function isValidSubmit(): object
    {
        return json_decode($this->recaptchaValidation->verify());
    }
}