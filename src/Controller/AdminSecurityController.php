<?php

namespace App\Controller;

use App\Controller\Route\Admin\AdminSecurity;
use App\Controller\Route\Admin\BaseAdminTrait;
use App\Controller\Route\Language\LanguageController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminSecurityController extends AbstractController
{
    use BaseAdminTrait;

    /**
     * @Route("/login/{slug}", name="app_login")
     * @param string $slug
     * @param AuthenticationUtils $authenticationUtils
     * @param TranslatorInterface $translator
     * @param Request $request
     * @return Response
     */
    public function login(string $slug, AuthenticationUtils $authenticationUtils, TranslatorInterface $translator, Request $request): Response
    {
        if ($this->isNotValidLoginUri($slug))
            return $this->redirectToRoute('index', [], 302);

        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig',
            [
                'active' => 'login',
                'admin' => '/admin/' . AdminSecurity::UUID,
                'last_username' => $lastUsername,
                'error' => $error
            ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout(): Response
    {
        return $this->redirectToRoute('index');
    }
}
