<?php


namespace App\Controller\Route\Checkout;


use App\Entity\DeliveryDays;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

trait CheckoutTrait
{
    /**
     * @param Request $request
     * @return array
     */
    private function retrieveSubmittedOrderInfo(Request $request)
    {
        $map = [];
        foreach (self::$orderSessionKeyList as $key) {

            $map[$key] = $this->retrieveSubmitted($request, $key);
        }
        return $map;
    }

    /**
     * @param Request $request
     * @param string $key
     * @return string|array|null
     */
    private function retrieveSubmitted(Request $request, string $key)
    {
        $varInPost = $request->request->get($key);
        if ($varInPost) {

            $request->getSession()->set($key, $varInPost);
            return $varInPost;
        }
        $varInSession = $request->getSession()->get($key);
        if ($varInSession) {

            return $varInSession;
        }
        $request->getSession()->set($key, null);
        return null;
    }

    /**
     * @param $cart
     * @param $catalog
     * @return array
     */
    private function mapCart($cart, $catalog): array
    {
        $map = [
            'items' => [],
            'count' => 0,
            'amount' => 0,
        ];

        foreach ($cart as $id) {
            if (!isset($map['items'][$id])) {
                $price =
                    $catalog[$id]['price_pack'] ??
                    $catalog[$id]['price_kilo'] ??
                    $catalog[$id]['price_unit'];
                $map['items'][$id] = array(
                    'qty' => 0,
                    'amount' => 0,
                    'name' => $catalog[$id]['name'],
                    'price' => $price,
                    'description' => $catalog[$id]['description'] ?? null,
                    'image' => $catalog[$id]['image_file'],
                );
                foreach ($cart as $elem) {

                    if ($elem === $id) {
                        $map['items'][$elem]['qty'] ++;
                        $map['items'][$elem]['amount'] += (float)$map['items'][$elem]['price'];
                        $map['count'] ++;
                        $map['amount'] += (float)$map['items'][$elem]['price'];
                    }
                }
            }
        }
        return $map;
    }

    /**
     * @param $catalog
     * @return array
     */
    private static function mergeCatalogSections($catalog): array
    {
        $items = [];
        foreach ($catalog as $section) {
            $items = array_merge(
                $items,
                $section
            );
        }
        return $items;
    }

    /**
     * @param array $misc
     * @param string $root
     * @return Response
     */
    private function renderEmptyOrderTemplate(array $misc, string $root): Response
    {
        return $this->render(
            'order/empty.html.twig',
            array_merge(
                $misc,
                [
                    'index_url' => $root,
                ]
            )
        );
    }

    /**
     * @param Request $request
     * @param array $order
     */
    private function resetOrderSession(Request $request, array $order): void
    {
        foreach ($order as $key) {

            if ($request->getSession()->get($key) !== null)
                $request->getSession()->remove($key);
        }
    }

    /**
     * @param int $timestamp
     * @return string[]
     */
    private static function getDeliveryDateMap(int $timestamp): array
    {
        return array(
            'weekday' => date('l', $timestamp),
            'day' => date('j', $timestamp),
            'month' => date('F', $timestamp),
            'year' => date('Y', $timestamp),
            'date' => date('j-m-Y', $timestamp),
        );
    }

    /**
     * @return array
     */
    private function getDeliveryDays(): array
    {
        $info = $this->getDoctrine()->getRepository(DeliveryDays::class)->find('1');
        $deliveryDays = [];
        foreach (['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'] as $weekday) {
            $method = 'get' . $weekday;
            if ($info->$method()) $deliveryDays[] = $weekday;
        }
        return $deliveryDays;
    }
}
