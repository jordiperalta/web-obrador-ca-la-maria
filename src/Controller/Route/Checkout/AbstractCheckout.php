<?php


namespace App\Controller\Route\Checkout;


abstract class AbstractCheckout
{
    const CART_FIELD_INDEX = 'cart';
    const PERSONAL_FIELD_INDEX = 'personal';
    const INVOICE_FIELD_INDEX = 'invoice';
    const DELIVERY_FIELD_INDEX = 'delivery';
    const ORDER_FIELD_INDEX = 'order';
    const ORDER_ID_FIELD_INDEX = 'order_id';
    const UUID_FIELD_INDEX = 'uuid';
}