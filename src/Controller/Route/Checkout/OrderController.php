<?php


namespace App\Controller\Route\Checkout;


use App\Controller\Route\Language\LanguageController;
use App\Controller\Route\RenderMapTrait;
use App\Kernel;
use App\Model\Bridge\PostalCode\PostalCodeModelBridge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class OrderController extends AbstractController
{
    use RenderMapTrait;
    use CheckoutTrait;

    /**
     * @var string[]
     */
    private static array $orderSessionKeyList = [
        AbstractCheckout::PERSONAL_FIELD_INDEX,
        AbstractCheckout::INVOICE_FIELD_INDEX,
        AbstractCheckout::DELIVERY_FIELD_INDEX,
        AbstractCheckout::ORDER_FIELD_INDEX,
    ];

    /**
     * @param TranslatorInterface $translator
     * @param Request $request
     * @param Kernel $kernel
     * @return Response
     * @throws BadRequestException
     */
    public function index(TranslatorInterface $translator, Request $request, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $root = $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL);
        $misc = $this->getRenderMap($lang, $request, $kernel);
        $form = $this->retrieveSubmittedOrderInfo($request);

        $cartList = json_decode($this->retrieveSubmitted($request, AbstractCheckout::CART_FIELD_INDEX));

        if (empty($cartList)) {

            return $this->renderEmptyOrderTemplate($misc, $root);
        }

        $post = $request->request->all();
        $cart = $this->mapCart(
            $cartList,
            self::mergeCatalogSections($misc['catalog'])
        );

        $translator->setLocale($lang);

        return $this->render(
            'order/order.html.twig',
            array_merge(
                $misc,
                [
                    'index_url' => $root,
                    'post' => $post,
                    'cart' => $cart,
                    'form' => $form,
                    'days' => $this->getDeliveryDateOptions(),
                    'provincias' => PostalCodeModelBridge::$provincias,
                ]
            )
        );
    }

    /**
     * @return array[]
     */
    private function getDeliveryDateOptions(): array
    {
        $date = $today = strtotime("+2 day");
        $pick = [];
        $deli = [];
        for ($x = 0; $x < 29; $x++) {

            $weekday = date('l', $date);

            $pick[$x] = array(
                'weekday' => $weekday,
                'day' => date('j', $date),
                'month' => date('F', $date),
                'year' => date('Y', $date),
                'date' => date('j-m-Y', $date),
            );

            if (in_array($weekday, $this->getDeliveryDays()))
                array_push($deli, $pick[$x]);

            $date += (60 * 60 * 24);
        }

        return array (
            'pick' => $pick,
            'deli' => $deli
        );
    }
}