<?php


namespace App\Controller\Route\Checkout;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class ResetController extends AbstractController
{
    use CheckoutTrait;

    /**
     * @var string[]
     */
    private static array $order = [
        AbstractCheckout::CART_FIELD_INDEX,
        AbstractCheckout::ORDER_FIELD_INDEX,
        AbstractCheckout::ORDER_ID_FIELD_INDEX,
        AbstractCheckout::UUID_FIELD_INDEX,
    ];

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function index(Request $request): RedirectResponse
    {
        $this->resetOrderSession($request, self::$order);
        return $this->redirectToRoute('index');
    }
}