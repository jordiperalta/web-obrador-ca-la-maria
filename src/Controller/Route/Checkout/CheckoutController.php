<?php


namespace App\Controller\Route\Checkout;


use App\Controller\Route\Language\LanguageController;
use App\Controller\Route\Payment\TransactionTrait;
use App\Controller\Route\RenderMapTrait;
use App\Controller\Services\Redsys\RedsysRedirectService;
use App\Entity\Order;
use App\Entity\SiteInfo;
use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class CheckoutController
 * @package App\Controller\Route\Checkout
 */
class CheckoutController extends AbstractController
{
    use RenderMapTrait;
    use CheckoutTrait;
    use TransactionTrait;

    /**
     * @var string[]
     */
    private static array $orderSessionKeyList = [
        AbstractCheckout::CART_FIELD_INDEX,
        AbstractCheckout::PERSONAL_FIELD_INDEX,
        AbstractCheckout::INVOICE_FIELD_INDEX,
        AbstractCheckout::ORDER_FIELD_INDEX,
        AbstractCheckout::DELIVERY_FIELD_INDEX,
    ];

    /**
     * @var string
     */
    private string $lang;

    /**
     * @var bool
     */
    private bool $gatewayProductionMode;

    /**
     * @param TranslatorInterface $translator
     * @param Request $request
     * @param Kernel $kernel
     * @return Response
     */
    public function index(Request $request, Kernel $kernel, TranslatorInterface $translator): Response
    {
        $this->lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $root = $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL);
        $misc = $this->getRenderMap($this->lang, $request, $kernel);
        $form = $this->retrieveSubmittedOrderInfo($request);

        $translator->setLocale($this->lang);

        if (empty($form['order'])) {

            return $this->renderEmptyOrderTemplate($misc, $root);
        }

        $items = json_decode($form[AbstractCheckout::CART_FIELD_INDEX]);

        if (empty($items)) {

            return $this->renderEmptyOrderTemplate($misc, $root);
        }

        $cart = $this->mapCart(
            $items,
            $this->mergeCatalogSections($misc['catalog'])
        );

        $form['order']['shipment-price'] = (int)$this->calculateShipmentPrice($cart) / 100;

        $this->deliveryRenderInfo($request, $form);
        $this->gatewayProductionMode = $this->paymentGatewayOnProductionMode($kernel, $this->getDoctrine());
        $order = $this->orderData(
            $request,
            $cart,
            $form,
            $this->getDoctrine()->getRepository(SiteInfo::class)->find(1)
        );

        return $this->render(
            'order/checkout.html.twig',
            array_merge(
                $misc,
                [
                    'index_url' => $root,
                    'form' => $form,
                    'cart' => $cart,
                    'order' => $order,
                    'date' => self::getDeliveryDateMap(strtotime($form['order']['date'])),
                    'redsys' => $this->redsysRedirectionParams($order, $request),
                    'pay' => $this->paymentButton($translator, $request),
                ]
            )
        );
    }

    private function calculateShipmentPrice(array $cart)
    {
        $siteInfo = $this->getDoctrine()->getRepository(SiteInfo::class)->find(1);
        return (float)$cart['amount'] >= 50 ? $siteInfo->getShippingPrice2() : $siteInfo->getShippingPrice();
    }

    /**
     * @param TranslatorInterface $translator
     * @param Request $request
     * @return array
     */
    private function paymentButton(TranslatorInterface $translator, Request $request): array
    {
        $buttonDisabled = '';
        $buttonText = $this->gatewayProductionMode ? 'pagar' : 'pagar (modo test)';

        if (!$this->gatewayProductionMode && in_array($request->server->get('SERVER_NAME'), [
            'obrador-ca-la-maria.com',
            'www.obrador-ca-la-maria.com']))
            $buttonDisabled = 'disabled';

        return [
            'text' => $translator->trans($buttonText),
            'disabled' => $buttonDisabled,
        ];
    }

    /**
     * @param SiteInfo $siteInfo
     * @param array $form
     * @param array $cart
     * @return float
     */
    private function getDeliveryPrice(SiteInfo $siteInfo, array $form, array $cart): float
    {
        if ($this->orderToBePickedAtShop($form))
            return 0;

        return (float)$cart['amount'] >= $siteInfo->getShippingFreeThreshold() ?
            $siteInfo->getShippingPrice2() / 100 :
            $siteInfo->getShippingPrice() / 100;
    }

    /**
     * @param array $form
     * @return bool
     */
    private function orderToBePickedAtShop(array $form): bool
    {
        return $form[AbstractCheckout::DELIVERY_FIELD_INDEX] === 'pick';
    }

    /**
     * @param Request $request
     * @param array $cart
     * @param array $form
     * @param SiteInfo $siteInfo
     * @return float[]
     */
    private function orderData(Request $request, array $cart, array $form, SiteInfo $siteInfo): array
    {
        $deli = $this->getDeliveryPrice($siteInfo, $form, $cart);
        $sAddr = $this->orderToBePickedAtShop($form) ? [] : $form[AbstractCheckout::DELIVERY_FIELD_INDEX];
        $total = (float)$cart['amount'] + $deli;
        $uuid = $request->getSession()->get(AbstractCheckout::UUID_FIELD_INDEX) ?? (string)Uuid::v1();
        $order_id = explode('-',$uuid)[1] . substr((string)intval(microtime(true)*1000),-8);
        $request->getSession()->set(AbstractCheckout::ORDER_ID_FIELD_INDEX, $order_id);
        $request->getSession()->set(AbstractCheckout::UUID_FIELD_INDEX, $uuid);

        $this->createOrder([
            'uuid' => $uuid,
            'order_id' => $order_id,
            'amount' => $total * 100,
            'basket' => $cart,
            'personal' => $form[AbstractCheckout::PERSONAL_FIELD_INDEX],
            'iAddr' => $form[AbstractCheckout::INVOICE_FIELD_INDEX],
            'sAddr' => $sAddr ?? [],
            'delivery_date' => $form[AbstractCheckout::ORDER_FIELD_INDEX]['date'],
            'type' => $this->gatewayProductionMode ? 'REAL' : 'TEST'
        ]);

        return array(
            'delivery' => $deli,
            'total' => $total,
            AbstractCheckout::ORDER_ID_FIELD_INDEX => $order_id,
            AbstractCheckout::UUID_FIELD_INDEX => $uuid,
        );
    }

    /**
     * @param float|string[] $order
     * @param Request $request
     * @return string[]
     */
    private function redsysRedirectionParams(array $order, Request $request): array
    {
        $amount = $order['total'] * 100;
        $homepage = $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL);

        return (new RedsysRedirectService())
            ->setFeedbackURLs($homepage, $request)
            ->setAmount($amount)
            ->setOrder($order[AbstractCheckout::ORDER_ID_FIELD_INDEX])
            ->getSendOutputVars($this->gatewayProductionMode, $request);
    }

    /**
     * @param array $vars
     * @return CheckoutController
     */
    private function createOrder(array $vars): CheckoutController
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository = $this->getDoctrine()->getRepository(Order::class);

        $order = $repository->findOneBy(['uuid' => $vars['uuid']]);

        if ($order)
        {
            $this->setOrderVars($order, $vars);
            $entityManager->flush();

            return $this;
        }

        $order = new Order();
        $this->setOrderVars($order, $vars);
        $order->setUuid($vars['uuid']);

        $entityManager->persist($order);
        $entityManager->flush();

        return $this;
    }

    /**
     * @param Order $order
     * @param array $vars
     * @return CheckoutController
     */
    private function setOrderVars(Order $order, array $vars): CheckoutController
    {
        $order
            ->setLang($this->lang)
            ->setOrderId($vars['order_id'])
            ->setAmount($vars['amount'])
            ->setBasket($vars['basket'])
            ->setPersonalInfo($vars['personal'])
            ->setInvoiceAddress($vars['iAddr'])
            ->setShippingAddress($vars['sAddr'])
            ->setDeliveryDate($vars['delivery_date'])
            ->setStatus('PENDING')
            ->setType($vars['type']);
        return $this;
    }

    /**
     * @param Request $request
     * @param array $form
     */
    private function deliveryRenderInfo(Request $request, array &$form)
    {
        if (strtoupper($request->getMethod()) === 'POST') {
            $form['delivery'] = $request->request->get('delivery');
        }

        if (!$form['delivery']) {
            $form['delivery'] = 'pick';
            $request->getSession()->set('delivery', []);
        }
    }
}
