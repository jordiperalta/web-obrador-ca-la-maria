<?php


namespace App\Controller\Route\API;


use App\Kernel;
use App\Model\Bridge\PostalCode\PostalCodeDbBridge;
use App\Model\Bridge\PostalCode\PostalCodeModelBridge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PostalCodeController extends AbstractController
{
    /**
     * @param string $id
     * @return Response
     */
    public function provincia(string $id): Response
    {
        $prefix = substr($id, 0 , 2);
        foreach (PostalCodeModelBridge::$provincias as $prov) {

            if ($prov['id'] === $prefix)
                return new JsonResponse($prov, Response::HTTP_OK);

        }
        return new JsonResponse(['error' => 'not found'], Response::HTTP_NOT_FOUND);
    }

    /**
     * @return Response
     */
    public function provinciasAll(): Response
    {
        return new JsonResponse(PostalCodeModelBridge::$provincias, Response::HTTP_OK);
    }

    /**
     * @param string $id
     * @param Kernel $kernel
     * @return Response
     */
    public function poblacion(string $id, Kernel $kernel): Response
    {
        if (!is_numeric($id))
            return new JsonResponse([
                'error' => 'not found',
                'valid' => false
            ], Response::HTTP_BAD_REQUEST);

        $code = (int)$id;

        $dataManager = method_exists($this, 'getDoctrine') ?
            new PostalCodeDbBridge($kernel, $this->getDoctrine()) :
            new PostalCodeModelBridge();

        foreach ($dataManager->towns() as $town) {
            if ($code >= (int)$town['min'] and $code <= (int)$town['max'])
                return new JsonResponse([
                    'code' => $id,
                    'city' => $town['loc'],
                    'valid' => true,
                ], Response::HTTP_OK );
        }

        return new JsonResponse([
            'error' => 'not found',
            'valid' => false
        ], Response::HTTP_OK);
    }
}
