<?php


namespace App\Controller\Route\Language;


use App\Controller\Route\RenderMapTrait;
use App\Kernel;
use App\Model\Bridge\Language\LanguageBridge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class LanguageController extends AbstractController
{
    use RenderMapTrait;

    /**
     * @const string
     */
    const LANGUAGE_CODE_ES = 'es';

    /**
     * @const string
     */
    const LANGUAGE_CODE_CA = 'ca';

    /**
     * @const string
     */
    const LANGUAGE_CODE_EN = 'en';

    /**
     * @var string []
     */
    private static array $acceptedLanguages = array(
        self::LANGUAGE_CODE_ES,
        self::LANGUAGE_CODE_CA,
        self::LANGUAGE_CODE_EN
    );

    /**
     * @var string
     */
    private static ?string $lang;

    /**
     * @var LanguageBridge
     */
    private LanguageBridge $repository;

    /**
     * @var TranslatorInterface
     */
    private TranslatorInterface $translator;

    /**
     * @param string $lang
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param Kernel $kernel
     * @return Response
     */
    public function configure(string $lang, Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        if (!self::isAcceptedLanguage($lang)) {

            throw new NotFoundHttpException('Page not found', null, 404);
        }

        $this->instance($lang, $request, $translator);
        $this->setValue($lang);
        $translator->setLocale($lang);

        return $this->render(
            'main/index.html.twig',
            array_merge(
                $this->getRenderMap($lang, $request, $kernel),
                [
                    'index_url' => $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL),
                    'delivery_days' => $this->listDeliveryDays($translator)
                ]
            )
        );
    }

    /**
     * @param string $lang
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return LanguageController
     */
    public function instance(?string $lang, Request $request, TranslatorInterface $translator)
    {
        self::$lang = $lang;
        $this->repository = new LanguageBridge($request);
        $this->translator = $translator;

        return $this;
    }

    /**
     * @param string $lang
     * @return LanguageController
     */
    public function setValue(string $lang): LanguageController
    {
        $this->repository->setDefault($lang);
        self::$lang = $lang;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return self::$lang ?? $this->getRepositoryLanguage();
    }

    /**
     * @param string $lang
     * @return bool
     */
    private static function isAcceptedLanguage($lang): bool
    {
        return in_array(strtolower($lang),self::$acceptedLanguages);
    }

    /**
     * @return string
     */
    private function getRepositoryLanguage(): string
    {
        return $this->repository->getCurrent() ?? $this->translator->getLocale();
    }
}
