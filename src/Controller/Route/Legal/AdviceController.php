<?php


namespace App\Controller\Route\Legal;


use App\Controller\Route\Language\LanguageController;
use App\Controller\Services\SiteInfo\SiteInfoService;
use App\Kernel;
use App\Model\Bridge\Messages\MessagesBridge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdviceController extends AbstractController
{
    public function privacy(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render('legal/privacy-wrapper.html.twig', [
                'lang' => $lang,
                'info' => SiteInfoService::getData($lang, $kernel, $this->getDoctrine()),
                'messages' => MessagesBridge::all($kernel),
            ]
        );
    }

    public function privacyModal(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render('legal/privacy-content.html.twig', [
                'lang' => $lang,
                'info' => SiteInfoService::getData($lang, $kernel, $this->getDoctrine()),
                'messages' => MessagesBridge::all($kernel),
            ]
        );
    }

    public function cookies(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render('legal/cookies-wrapper.html.twig', [
                'lang' => $lang,
                'info' => SiteInfoService::getData($lang, $kernel, $this->getDoctrine()),
                'messages' => MessagesBridge::all($kernel),
            ]
        );
    }

    public function cookiesModal(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render('legal/cookies-content.html.twig', [
                'lang' => $lang,
                'info' => SiteInfoService::getData($lang, $kernel, $this->getDoctrine()),
                'messages' => MessagesBridge::all($kernel),
            ]
        );
    }

    public function terms(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render('legal/terms-wrapper.html.twig', [
                'lang' => $lang,
                'info' => SiteInfoService::getData($lang, $kernel, $this->getDoctrine()),
                'messages' => MessagesBridge::all($kernel),
            ]
        );
    }

    public function termsModal(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render('legal/terms-content.html.twig', [
                'lang' => $lang,
                'info' => SiteInfoService::getData($lang, $kernel, $this->getDoctrine()),
                'messages' => MessagesBridge::all($kernel),
            ]
        );
    }
}