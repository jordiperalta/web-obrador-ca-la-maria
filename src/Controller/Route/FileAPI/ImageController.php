<?php


namespace App\Controller\Route\FileAPI;


use App\Kernel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ImageController
{
    /**
     * string
     */
    const DEFAULT_IMAGE_FILE = 'default.svg';

    /**
     * @var string
     */
    protected static string $data_directory = 'data' . DIRECTORY_SEPARATOR . 'img';

    /**
     * @param string|null $filename
     * @param Kernel $kernel
     * @return BinaryFileResponse
     */
    public function read(?string $filename, Kernel $kernel)
    {
        $path = $kernel->getProjectDir() . DIRECTORY_SEPARATOR . self::$data_directory;
        $filename = $filename ?? self::DEFAULT_IMAGE_FILE;

        $fileContent = file_exists( $path . DIRECTORY_SEPARATOR . $filename ) ?
            $path . DIRECTORY_SEPARATOR . $filename :
            $path . DIRECTORY_SEPARATOR . self::DEFAULT_IMAGE_FILE;

        return new BinaryFileResponse($fileContent);
    }

    /**
     * @param string $filename
     * @param Kernel $kernel
     * @return array
     */
    public static function info(string $filename, Kernel $kernel)
    {
        $file = $kernel->getProjectDir() . DIRECTORY_SEPARATOR . self::$data_directory . DIRECTORY_SEPARATOR . $filename;
        $isSet = file_exists($file);
        $size = $isSet ? '(' . sprintf("%.1f",filesize($file)/1024) . 'Kb)' : '';

        return array(
            'name' => $filename,
            'isset' => $isSet,
            'size' => $size
        );
    }
}