<?php


namespace App\Controller\Route\Admin;


use App\Controller\Route\Checkout\CheckoutTrait;
use App\Entity\Order;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AdminOrderController extends AbstractController
{
    use BaseAdminTrait;
    use CheckoutTrait;

    /**
     * @param string $slug
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param UserInterface $user
     * @return Response
     */
    public function list(string $slug, Request $request, TranslatorInterface $translator/*, UserInterface $user*/): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

//        var_dump($user->getRoles(), $user->getUsername());

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $page = $request->query->get('page');

        $translator->setLocale('es');

        $repo = $this->getDoctrine()->getRepository(Order::class);
        $count = $repo->countCompletedOrders();
        $list = $repo->findCompletedOrdersByLimit($page);

        return $this->render('admin/order-list.html.twig', array(
            'h2title' => 'Pedidos',
            'list' => $list,
            'date' => array_map(fn($order) => self::getDeliveryDateMap(strtotime($order->getDeliveryDate())), $list),
            'count' => $count,
            'page' => (int)$page,
            'pages' => min( [(int)ceil($count / OrderRepository::MAX_RESULTS) - 1, 9] ),
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'order' ));
    }

    /**
     * @param string $slug
     * @param Request $request
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function search(string $slug, Request $request, TranslatorInterface $translator): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $page = $request->query->get('page');
        $field = $request->query->get('field');
        $value = $request->query->get('value');

        $translator->setLocale('es');

        $repo = $this->getDoctrine()->getRepository(Order::class);
        $count = $repo->countOrdersMatching($field, $value);
        $list = $repo->searchOrderByField($field, $value);

        return $this->render('admin/order-list.html.twig', array(
            'h2title' => 'Pedidos',
            'list' => $list,
            'date' => array_map(fn($order) => self::getDeliveryDateMap(strtotime($order->getDeliveryDate())), $list),
            'query' => 'field='.$field.'&value='.$value.'&',
            'count' => $count,
            'page' => (int)$page,
            'pages' => min( [(int)ceil($count / OrderRepository::MAX_RESULTS) - 1, 9] ),
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'order' ));
    }

    /**
     * @param string $slug
     * @param string $uuid
     * @return Response
     */
    public function read(string $slug, string $uuid): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $order = $this->getDoctrine()->getRepository(Order::class)->findOneBy(['uuid' => $uuid]);

        if (!$order)
            throw new NotFoundHttpException('Order not found', null, 404);

        return $this->render('admin/order.html.twig', array(
            'h2title' => 'Pedido',
            'order' => $order,
            'date' => self::getDeliveryDateMap(strtotime($order->getDeliveryDate())),
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'order' ));
    }
}