<?php


namespace App\Controller\Route\Admin;


use App\Security\AdminLoginAuthenticator;

Trait BaseAdminTrait
{
    protected function isNotValidAdminUri(string $slug): bool
    {
        return $slug !== AdminSecurity::UUID;
    }

    protected function isNotValidLoginUri(string $slug): bool
    {
        return $slug !== AdminLoginAuthenticator::UUID;
    }
}