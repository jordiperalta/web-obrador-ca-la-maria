<?php


namespace App\Controller\Route\Admin;


use App\Entity\DeliveryDays;
use App\Service\LoggerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class DeliveryDaysController extends AbstractController
{
    use BaseAdminTrait;

    public function read(string $slug, TranslatorInterface $translator): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $info = $this->getDoctrine()->getRepository(DeliveryDays::class)->findAll()[0];

        $data = [
            'monday' => [
                'definition' => $translator->trans('Monday'),
                'value' => $info->getMonday(),
            ],
            'tuesday' => [
                'definition' => $translator->trans('Tuesday'),
                'value' => $info->getTuesday(),
            ],
            'wednesday' => [
                'definition' => $translator->trans('Wednesday'),
                'value' => $info->getWednesday(),
            ],
            'thursday' => [
                'definition' => $translator->trans('Thursday'),
                'value' => $info->getThursday(),
            ],
            'friday' => [
                'definition' => $translator->trans('Friday'),
                'value' => $info->getFriday(),
            ],
            'saturday' => [
                'definition' => $translator->trans('Saturday'),
                'value' => $info->getSaturday(),
            ],
            'sunday' => [
                'definition' => $translator->trans('Sunday'),
                'value' => $info->getSunday(),
            ],
        ];

        return $this->render('admin/delivery-info.html.twig',array(
            'admin' => '/admin/' . AdminSecurity::UUID,
            'info' => $data,
            'active' => 'delivery-info',
        ));
    }

    public function save(string $slug, Request $request): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $data = $request->request->all();

        $entityManager = $this->getDoctrine()->getManager();
        $info = $entityManager->getRepository(DeliveryDays::class)->find('1');

        $info->setMonday(isset($data['monday']))
            ->setTuesday(isset($data['tuesday']))
            ->setWednesday(isset($data['wednesday']))
            ->setThursday(isset($data['thursday']))
            ->setFriday(isset($data['friday']))
            ->setSaturday(isset($data['saturday']))
            ->setSunday(isset($data['sunday']));

        $entityManager->flush();
        $logger = new LoggerService($this->getDoctrine(), $request);
        $logger->log($this->getUser(), 'UPDATE', 'DELIVERY');

        return $this->redirectToRoute('delivery-info-read', ['slug' => $slug] );
    }
}