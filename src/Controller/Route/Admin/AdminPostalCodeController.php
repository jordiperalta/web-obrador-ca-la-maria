<?php


namespace App\Controller\Route\Admin;


use App\Entity\PostalCode;
use App\Kernel;
use App\Repository\PostalCodeRepository;
use App\Service\LoggerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Uid\Uuid;

class AdminPostalCodeController extends AbstractController
{
    use BaseAdminTrait;

    /**
     * @var PostalCode
     */
    private ?PostalCode $location;

    /**
     * @var string
     */
    private string $uuid;

    /**
     * @param string $slug
     * @param Request $request
     * @return Response
     */
    public function list(string $slug, Request $request): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $page = (int)$request->query->get('page');

        $repo = $this->getDoctrine()->getRepository(PostalCode::class);
        $count = $repo->countAll();

        return $this->render('admin/postalcode-list.html.twig', array(
            'h2title' => 'Código Postal Envíos',
            'list' => $repo->findAllOrderByLimit('loc', 'ASC', $page),
            'count' => $count,
            'page' => (int)$page,
            'pages' => (int)ceil($count / PostalCodeRepository::MAX_RESULTS) - 1,
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'site-info' ));
    }

    /**
     * @param string $slug
     * @param Kernel $kernel
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function addNew(string $slug, Kernel $kernel, Request $request, SessionInterface $session): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $info = [
            'loc' => [
                'definition' => 'Nombre del municipio',
                'value' => null,
                'required' => true,
                'col' => 'col-5',
            ],
            'min' => [
                'definition' => 'código postal <b>mínimo</b>',
                'value' => null,
                'type' => 'tel',
                'required' => true,
                'pattern' => '[0-9]{5}',
                'maxlength' => '5',
                'title' => 'Introduce 5 dígitos entre el 0 y el 9',
            ],
            'max' => [
                'definition' => 'código postal <b>máximo</b>',
                'value' => null,
                'type' => 'tel',
                'required' => true,
                'pattern' => '[0-9]{5}',
                'maxlength' => '5',
                'title' => 'Introduce 5 dígitos entre el 0 y el 9',
            ],
        ];

        $this->listPageFeedback('admin-postalcode-list-page', $request, $session);

        return $this->render('admin/postalcode.html.twig',array(
            'h2title' => 'Añadir nueva localidad y rango de códigos postales para envío',
            'action' => '/admin/' . AdminSecurity::UUID . '/postalcode/create',
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'postalcode',
            'page' => $session->get('admin-postalcode-list-page'),
            'info' => $info,
        ));
    }

    /**
     * @param string $slug
     * @param string $uuid
     * @param Kernel $kernel
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function read(string $slug, string $uuid, Kernel $kernel, Request $request, SessionInterface $session): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $data = $this->getDoctrine()->getRepository(PostalCode::class)->findOneBy(['uuid' => $uuid]);

        if (!$data) {
            throw $this->createNotFoundException(
                'No postal code found for id '.$uuid
            );
        }

        $info = [
            'loc' => [
                'definition' => 'Nombre del municipio',
                'value' => $data->getLoc(),
                'required' => true,
                'col' => 'col-5',
            ],
            'min' => [
                'definition' => 'código postal <b>mínimo</b>',
                'value' => $data->getMin(),
                'type' => 'tel',
                'required' => true,
                'pattern' => '[0-9]{5}',
                'maxlength' => '5',
                'title' => 'Introduce 5 dígitos entre el 0 y el 9',
            ],
            'max' => [
                'definition' => 'código postal <b>máximo</b>',
                'value' => $data->getMax(),
                'type' => 'tel',
                'required' => true,
                'pattern' => '[0-9]{5}',
                'maxlength' => '5',
                'title' => 'Introduce 5 dígitos entre el 0 y el 9',
            ],
        ];

        $this->listPageFeedback('admin-postalcode-list-page', $request, $session);

        return $this->render('admin/postalcode.html.twig',array(
            'h2title' => 'Editar código postal envíos a domicilio',
            'action' => '/admin/' . AdminSecurity::UUID . '/postalcode/' . $uuid . '/update',
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'postalcode',
            'page' => $session->get('admin-postalcode-list-page'),
            'info' => $info,
        ));
    }

    /**
     * @param string $slug
     * @param Request $request
     * @return Response
     */
    public function create(string $slug, Request $request): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $entityManager = $this->getDoctrine()->getManager();

        $uuid = Uuid::v4();
        $this->location = new PostalCode();
        $this->location->setUuid($uuid);
        $this->setEntity($request->request);

        $entityManager->persist($this->location);
        $entityManager->flush();

        $logger = new LoggerService($this->getDoctrine(), $request);
        $logger->log($this->getUser(), 'CREATE', 'POSTAL_CODE', $uuid);

        return $this->redirectToRoute('admin-postalcode-read', ['slug' => $slug, 'uuid' => $uuid ] );
    }

    /**
     * @param string $slug
     * @param string $uuid
     * @param Request $request
     * @return Response
     */
    public function update(string $slug, string $uuid, Request $request): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $entityManager = $this->getDoctrine()->getManager();
        $this->location = $entityManager->getRepository(PostalCode::class)->findOneBy(['uuid' => $uuid]);

        if (!$this->location) {
            throw $this->createNotFoundException(
                'No postal code found for id '.$uuid
            );
        }

        $this->setEntity($request->request);

        $entityManager->persist($this->location);
        $entityManager->flush();

        $logger = new LoggerService($this->getDoctrine(), $request);
        $logger->log($this->getUser(), 'UPDATE','POSTAL_CODE', $uuid);

        return $this->redirectToRoute('admin-postalcode-read', ['slug' => $slug, 'uuid' => $uuid] );
    }

    /**
     * @param string $slug
     * @param string $uuid
     * @param SessionInterface $session
     * @return Response
     */
    public function deleteConfirm(string $slug, string $uuid, SessionInterface $session): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $location = $this->getDoctrine()->getRepository(PostalCode::class)->findOneBy(['uuid' => $uuid]);

        if (!$location) {
            throw $this->createNotFoundException(
                'No postal code found for id '.$uuid
            );
        }

        return $this->render('admin/postalcode-delete-confirm.html.twig',array(
            'h2title' => 'Eliminar localidad para envío a domicilio',
            'action' => '/admin/' . AdminSecurity::UUID . '/postalcode/' . $uuid . '/delete',
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'postalcode',
            'page' => $session->get('admin-postalcode-list-page'),
            'info' => $location,
        ));
    }

    /**
     * @param string $slug
     * @param string $uuid
     * @param Request $request
     * @return Response
     */
    public function delete(string $slug, string $uuid, Request $request): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $check = $request->request->get('uuid');

        if ($uuid !== $check) {
            throw new BadRequestException('Request not allowed, no UUID submitted', 400);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $location = $entityManager->getRepository(PostalCode::class)->findOneBy(['uuid' => $uuid]);

        if (!$location) {
            throw $this->createNotFoundException(
                'No postal code found for id '.$uuid
            );
        }

        $entityManager->remove($location);
        $entityManager->flush();

        $logger = new LoggerService($this->getDoctrine(), $request);
        $logger->log($this->getUser(), 'DELETE','POSTAL_CODE', $uuid);

        return $this->redirectToRoute('admin-postalcode-list', ['slug' => $slug] );
    }

    /**
     * @param InputBag $data
     * @return AdminPostalCodeController
     */
    private function setEntity(InputBag $data): AdminPostalCodeController
    {
        $this->location->setloc($data->get('loc'))
            ->setMin($data->get('min'))
            ->setMax($data->get('max'));
        return $this;
    }

    /**
     * @param string $index
     * @param Request $request
     * @param SessionInterface $session
     * @return AdminPostalCodeController
     */
    private function listPageFeedback(string $index, Request $request, SessionInterface $session): AdminPostalCodeController
    {
        $session->set($index,$request->query->get('page') ?? $session->get($index));
        return $this;
    }
}
