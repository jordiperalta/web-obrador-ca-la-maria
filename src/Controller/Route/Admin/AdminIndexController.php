<?php


namespace App\Controller\Route\Admin;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminIndexController extends AbstractController
{
    use BaseAdminTrait;

    public function homepage(string $slug): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('admin/admin.html.twig',array(
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'index'
        ));
    }
}