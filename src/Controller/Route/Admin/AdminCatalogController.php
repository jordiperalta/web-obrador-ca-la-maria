<?php


namespace App\Controller\Route\Admin;


use App\Controller\Services\Catalog\CatalogService;
use App\Entity\Catalog;
use App\Kernel;
use App\Repository\CatalogRepository;
use App\Service\LoggerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\InputBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Uid\Uuid;

class AdminCatalogController extends AbstractController
{
    use BaseAdminTrait;

    /**
     * @var string
     */
    private static string $imgDirectory = DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR;

    /**
     * @var Kernel
     */
    private Kernel $kernel;

    /**
     * @var Catalog
     */
    private ?Catalog $product;

    /**
     * @var string
     */
    private string $uuid;

    /**
     * @var int
     */
    private int $suffix;

    /**
     * @param string $slug
     * @param Request $request
     * @return Response
     */
    public function list(string $slug, Request $request): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $page = (int)$request->query->get('page');

        $repo = $this->getDoctrine()->getRepository(Catalog::class);
        $count = $repo->countAll();

        return $this->render('admin/catalog-list.html.twig', array(
            'h2title' => 'Productos del catálogo',
            'list' => $repo->findAllOrderByLimit('ref', 'ASC', $page),
            'count' => $count,
            'page' => (int)$page,
            'pages' => (int)ceil($count / CatalogRepository::MAX_RESULTS) - 1,
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'catalog' ));
    }

    /**
     * @param string $slug
     * @param Kernel $kernel
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function addNew(string $slug, Kernel $kernel, Request $request, SessionInterface $session): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $info1 = [
            'ref' => [
                'definition' => 'Referencia del producto*',
                'value' => null,
                'required' => true
            ],
            'section' => [
                'definition' => 'Sección catálogo*',
                'value' => null,
                'type' => 'select',
                'options' => $this->getCatalogSections($kernel)
            ],
            'publish' => [
                'definition' => 'Publicar elemento',
                'value' => true,
                'type' => 'checkbox'
            ],
            'name' => [
                'definition' => 'Nombre del producto*',
                'value' => null,
            ],
        ];
        $info2 = [
            'size' => [
                'definition' => 'Tamaño',
                'value' => null,
            ],
            'weight' => [
                'definition' => 'Peso',
                'value' => null,
            ],
            'units' => [
                'definition' => 'Unidades',
                'value' => null,
            ],
            'piece' => [
                'definition' => 'Empaquetado',
                'value' => null,
            ],
            'decorated' => [
                'definition' => 'Decoración',
                'value' => null,
            ],
            'cold' => [
                'definition' => 'Condiciones de conservación',
                'value' => null,
            ],
            'description' => [
                'definition' => 'Descripción',
                'value' => null,
                'col' => 'col-6',
            ],
            'description_options' => [
                'definition' => 'Lista de opciones de la descripción',
                'value' => [null, null, null],
                'col' => 'col-5'
            ],
            'iva' => [
                'definition' => 'IVA %',
                'value' => 10,
                'type' => 'select',
                'options' => [4,10,21],
                'col' => 'col',
            ],
            'price_unit' => [
                'definition' => 'Precio por unidad',
                'value' => null,
                'type' => 'number',
                'step' => '0.01',
                'col' => 'col',
            ],
            'price_kilo' => [
                'definition' => 'Precio / Kilo',
                'value' => null,
                'type' => 'number',
                'step' => '0.01',
                'col' => 'col',
            ],
            'price_pack' => [
                'definition' => 'Precio / Pack (opcional)',
                'value' => null,
                'type' => 'number',
                'step' => '0.01',
                'col' => 'col',
            ],
        ];
        $info3 = [
            'image_file' => [
                'definition' => 'Imagen',
                'value' => null,
                'type' => 'file',
                'col' => 'col-5',
            ]
        ];

        $this->listPageFeedback('admin-catalog-list-page', $request, $session);

        return $this->render('admin/catalog.html.twig',array(
            'h2title' => 'Añadir nuevo producto al catálogo',
            'action' => '/admin/' . AdminSecurity::UUID . '/catalog/create',
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'catalog',
            'page' => $session->get('admin-catalog-list-page'),
            'info1' => $info1,
            'info2' => $info2,
            'info3' => $info3,
        ));
    }

    /**
     * @param string $slug
     * @param string $uuid
     * @param Kernel $kernel
     * @param Request $request
     * @param SessionInterface $session
     * @return Response
     */
    public function read(string $slug, string $uuid, Kernel $kernel, Request $request, SessionInterface $session): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $info = $this->getDoctrine()->getRepository(Catalog::class)->findOneBy(['uuid' => $uuid]);

        if (!$info) {
            throw $this->createNotFoundException(
                'No product found for id '.$uuid
            );
        }

        $info1 = [
            'ref' => [
                'definition' => 'Referencia del producto*',
                'value' => substr($info->getRef(), 1),
                'required' => true
            ],
            'section' => [
                'definition' => 'Sección catálogo*',
                'value' => $info->getSection(),
                'type' => 'select',
                'options' => $this->getCatalogSections($kernel)
            ],
            'publish' => [
                'definition' => 'Publicar elemento',
                'value' => $info->getPublish(),
            ],
            'name' => [
                'definition' => 'Nombre del producto',
                'value' => $info->getName(),
            ],
        ];
        $info2 = [
            'size' => [
                'definition' => 'Tamaño',
                'value' => $info->getSize(),
            ],
            'weight' => [
                'definition' => 'Peso',
                'value' => $info->getWeight()
            ],
            'units' => [
                'definition' => 'Unidades',
                'value' => $info->getUnits()
            ],
            'piece' => [
                'definition' => 'Empaquetado',
                'value' => $info->getPiece()
            ],
            'decorated' => [
                'definition' => 'Decoración',
                'value' => $info->getDecorated()
            ],
            'cold' => [
                'definition' => 'Condiciones de conservación',
                'value' => $info->getCold()
            ],
            'description' => [
                'definition' => 'Descripción*',
                'value' => $info->getDescription(),
                'required' => true,
                'col' => 'col-12 col-md-6',
            ],
            'description_options' => [
                'definition' => 'Lista de opciones de la descripción',
                'value' => $info->getDescriptionOptions(),
                'col' => 'col-12 col-md-5',
            ],
            'iva' => [
                'definition' => 'IVA %',
                'value' => $info->getIva() ?? 10,
                'type' => 'select',
                'options' => [4,10,21],
                'col' => 'col-12 col-md',
            ],
            'price_unit' => [
                'definition' => 'Precio por unidad',
                'value' => sprintf("%.02f", $info->getPriceUnit()),
                'type' => 'number',
                'step' => '0.01',
                'col' => 'col-12 col-md',
            ],
            'price_kilo' => [
                'definition' => 'Precio / Kilo (opcional)',
                'value' => sprintf("%.02f", $info->getPriceKilo()),
                'type' => 'number',
                'step' => '0.01',
                'col' => 'col-12 col-md',
            ],
            'price_pack' => [
                'definition' => 'Precio / Pack (opcional)',
                'value' => sprintf("%.02f", $info->getPricePack()),
                'type' => 'number',
                'step' => '0.01',
                'col' => 'col-12 col-md',
            ],
        ];
        $info3 = [
            'image_file' => [
                'definition' => 'Imagen',
                'value' => $info->getImageFile(),
                'type' => 'file'
            ]
        ];

        $this->listPageFeedback('admin-catalog-list-page', $request, $session);

        return $this->render('admin/catalog.html.twig',array(
            'h2title' => 'Editar producto del catálogo',
            'action' => '/admin/' . AdminSecurity::UUID . '/catalog/' . $uuid . '/update',
            'admin' => '/admin/' . AdminSecurity::UUID,
            'active' => 'catalog',
            'page' => $session->get('admin-catalog-list-page'),
            'info1' => $info1,
            'info2' => $info2,
            'info3' => $info3,
        ));
    }

    /**
     * @param string $slug
     * @param Request $request
     * @param Kernel $kernel
     * @return Response
     */
    public function create(string $slug, Request $request, Kernel $kernel): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $ref = $request->request->get('ref');
        $id = 'i' . $ref;
        $find = $this->getDoctrine()->getRepository(Catalog::class)->findOneBy(['ref' => $id]);

        if ($find && $id === $find->getRef())
            return new Response("No es posible crear el producto. La referencia $ref ya existe.", 400);

        $uuid = Uuid::v4();
        $this->instanceUpdateEntityVars($uuid, $kernel);
        $entityManager = $this->getDoctrine()->getManager();

        $this->product = new Catalog();
        $this->product->setUuid($this->uuid);
        $this->setEntity($request->request);

        $this->product->setImageFile($this->upload($request->files->get('image')));

        $entityManager->persist($this->product);
        $entityManager->flush();

        $logger = new LoggerService($this->getDoctrine(), $request);
        $logger->log($this->getUser(), 'CREATE', 'CATALOG', $uuid);

        return $this->redirectToRoute('admin-catalog-read', ['slug' => $slug, 'uuid' => $this->uuid ] );
    }

    /**
     * @param string $slug
     * @param string $uuid
     * @param Request $request
     * @param Kernel $kernel
     * @return Response
     */
    public function update(string $slug, string $uuid, Request $request, Kernel $kernel): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $ref = $request->request->get('ref');
        $id = 'i' . $ref;
        $find = $this->getDoctrine()->getRepository(Catalog::class)->findOneBy(['ref' => $id]);

        if ($find && $id === $find->getRef() && $uuid !== $find->getUuid())
            return new Response("No es posible actualizar el producto. La referencia <b>$ref</b> ya existe en el producto {$find->getUuid()}: <b>{$find->getName()}</b>.", 400);

        $this->instanceUpdateEntityVars($uuid, $kernel);
        $entityManager = $this->getDoctrine()->getManager();

        $this->product = $entityManager->getRepository(Catalog::class)->findOneBy(['uuid' => $uuid]);
        $this->persistUpload($this->upload($request->files->get('image')));
        $this->setEntity($request->request);

        $entityManager->flush();

        $logger = new LoggerService($this->getDoctrine(), $request);
        $logger->log($this->getUser(), 'UPDATE', 'CATALOG', $uuid);

        return $this->redirectToRoute('admin-catalog-read', ['slug' => $slug, 'uuid' => $uuid] );
    }

    /**
     * @param string[]|null $uploads
     * @return AdminCatalogController
     */
    private function persistUpload(?array $uploads): AdminCatalogController
    {
        $persist = [];
        foreach ($this->product->getImageFile() ?? [null, null, null] as $key => $value)
        {
            $persist[] = $uploads[$key] ?? $value;
        }
        $this->product->setImageFile($persist);
        return $this;
    }

    /**
     * @param UploadedFile[] $images
     * @return string[]
     */
    private function upload(array $images): ?array
    {
        if (empty($images))
            return null;

        return array_map([$this, 'uploadImage'], $images);
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function uploadImage(UploadedFile $file): string
    {
        $fileName = substr($this->uuid, 0, 8) . '-0' . $this->suffix . '-' . $file->getClientOriginalName();

        $origin = $file->getPathname();
        $target = $this->kernel->getProjectDir() . self::$imgDirectory . $fileName;

        $this->suffix ++;

        if (!$this->uploadedFileIsImage($file))
            return '';

        move_uploaded_file($origin, $target);
        return $fileName;
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    private function uploadedFileIsImage(UploadedFile $file): bool
    {
        return explode('/', $file->getClientMimeType())[0] === 'image';
    }

    /**
     * @param string $uuid
     * @param Kernel $kernel
     * @return AdminCatalogController
     */
    private function instanceUpdateEntityVars(string $uuid, Kernel $kernel)
    {
        $this->uuid = $uuid;
        $this->kernel = $kernel;
        $this->suffix = 0;

        return $this;
    }

    /**
     * @param InputBag $data
     * @return AdminCatalogController
     */
    private function setEntity(InputBag $data): AdminCatalogController
    {
        $this->product->setRef('i' . $data->get('ref'))
            ->setSection($data->get('section'))
            ->setName($data->get('name'))
            ->setPublish((bool)($data->get('publish') ?? false))
            ->setSize($data->get('size'))
            ->setWeight($data->get('weight'))
            ->setUnits($data->get('units'))
            ->setPiece($data->get('piece'))
            ->setDecorated($data->get('decorated'))
            ->setCold($data->get('cold'))
            ->setDescription($data->get('description'))
            ->setDescriptionOptions((array)$data->get('description_options'))
            ->setIva((int)$data->get('iva'))
            ->setPriceUnit((float)$data->get('price_unit'))
            ->setPriceKilo((float)$data->get('price_kilo'))
            ->setPricePack((float)$data->get('price_pack'));
        return $this;
    }

    /**
     * @param Kernel $kernel
     * @return array
     */
    private function getCatalogSections(Kernel $kernel): array
    {
        return array_keys(CatalogService::getSections('es', $kernel, $this->getDoctrine()));
    }

    /**
     * @param string $index
     * @param Request $request
     * @param SessionInterface $session
     * @return AdminCatalogController
     */
    private function listPageFeedback(string $index, Request $request, SessionInterface $session): AdminCatalogController
    {
        $session->set($index,$request->query->get('page') ?? $session->get($index));
        return $this;
    }
}
