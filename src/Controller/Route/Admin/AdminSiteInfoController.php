<?php


namespace App\Controller\Route\Admin;


use App\Entity\SiteInfo;
use App\Service\LoggerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminSiteInfoController extends AbstractController
{
    use BaseAdminTrait;

    public function read(string $slug): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $info = $this->getDoctrine()->getRepository(SiteInfo::class)->findAll()[0];

        $data = [
            'company_official' => [
                'definition' => 'Nombre legal',
                'value' => $info->getCompanyOfficial(),
            ],
            'company_commercial' => [
                'definition' => 'Nombre comercial (texto legal)',
                'value' => $info->getCompanyCommercial(),
            ],
            'company_legal_short' => [
                'definition' => 'Nombre corto (texto legal)',
                'value' => $info->getCompanyLegalShort(),
            ],
            'web_site' => [
                'definition' => 'Dirección web (texto legal)',
                'value' => $info->getWebSite(),
            ],
            'web_domain' => [
                'definition' => 'Dominio web (texto legal)',
                'value' => $info->getWebDomain(),
            ],
            'CIF' => [
                'definition' => 'CIF',
                'value' => $info->getCIF()
            ],
            'registrar_data' => [
                'definition' => 'Datos de registro',
                'value' => $info->getRegistrarData()
            ],
            'address' => [
                'definition' => 'Dirección física tienda',
                'value' => $info->getAddress()
            ],
            'phone_link' => [
                'definition' => 'Enlace HTML teléfono (ej. 123-456-789)',
                'value' => $info->getPhoneLink()
            ],
            'phone_text' => [
                'definition' => 'Teléfono (texto visible, ej. 123 456 789)',
                'value' => $info->getPhoneText()
            ],
            'whatsapp_link' => [
                'definition' => 'Enlace HTML Whatsapp (sólo números, ej. 123456789)',
                'value' => $info->getWhatsappLink()
            ],
            'mobile_text' => [
                'definition' => 'Móvil (texto visible, ej. 123 456 789)',
                'value' => $info->getMobileText()
            ],
            'official_email' => [
                'definition' => 'Email oficial web (visible en información y condiciones)',
                'value' => $info->getOfficialEmail(),
                'type' => 'email'
            ],
            'admin_email' => [
                'definition' => 'Email envío formularios y pagos (interno tienda on-line)',
                'value' => $info->getAdminEmail(),
                'type' => 'email'
            ],
            'instagram_link' => [
                'definition' => 'Enlace Instagram',
                'value' => $info->getInstagramLink()
            ],
            'facebook_link' => [
                'definition' => 'Enlace Facebook',
                'value' => $info->getFacebookLink()
            ],
            'shipping_price' => [
                'definition' => 'Precio de envío base en € (ej. 4.95)',
                'value' => sprintf("%.02f", ((int)$info->getShippingPrice())/100),
                'type' => 'number',
                'step' => '0.01'
            ],
            'shipping_price_2' => [
                'definition' => 'Precio de envío reducido/gratuito',
                'value' => sprintf("%.02f", ((int)$info->getShippingPrice2())/100),
                'type' => 'number',
                'step' => '0.01'
            ],
            'shipping_free_threshold' => [
                'definition' => 'Envío reducido/gratuito a partir de € (ej. 50.00)',
                'value' => sprintf("%.02f", (int)$info->getShippingFreeThreshold()),
                'type' => 'number',
                'step' => '0.01'
            ],
            'gateway_env' => [
                'definition' => 'Modo de pago (pruebas/producción)',
                'value' => $info->getGatewayEnv(),
                'type' => 'select',
                'options' => [
                    'SANDBOX',
                    'PRODUCTION'
                ]
            ]
        ];

        return $this->render('admin/site-info.html.twig',array(
            'admin' => '/admin/' . AdminSecurity::UUID,
            'info' => $data,
            'active' => 'site-info',
        ));
    }

    public function save(string $slug, Request $request): Response
    {
        if ($this->isNotValidAdminUri($slug))
            return $this->redirectToRoute('index', [], 302);

        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $data = $request->request->all();

        $entityManager = $this->getDoctrine()->getManager();
        $info = $entityManager->getRepository(SiteInfo::class)->find('1');

        $info->setCompanyOfficial($data['company_official'])
            ->setCompanyCommercial($data['company_commercial'])
            ->setCompanyLegalShort($data['company_legal_short'])
            ->setWebSite($data['web_site'])
            ->setWebDomain($data['web_domain'])
            ->setCIF($data['CIF'])
            ->setRegistrarData($data['registrar_data'])
            ->setAddress($data['address'])
            ->setPhoneLink($data['phone_link'])
            ->setPhoneText($data['phone_text'])
            ->setWhatsappLink($data['whatsapp_link'])
            ->setMobileText($data['mobile_text'])
            ->setAdminEmail($data['admin_email'])
            ->setInstagramLink($data['instagram_link'])
            ->setFacebookLink($data['facebook_link'])
            ->setShippingPrice(((float)$data['shipping_price']) * 100)
            ->setShippingPrice2(((float)$data['shipping_price_2']) * 100)
            ->setShippingFreeThreshold(((float)$data['shipping_free_threshold']))
            ->setGatewayEnv($data['gateway_env']);

        $entityManager->flush();
        $logger = new LoggerService($this->getDoctrine(), $request);
        $logger->log($this->getUser(), 'UPDATE', 'SITE_INFO');

        return $this->redirectToRoute('admin-site-info-read', ['slug' => $slug] );
    }
}