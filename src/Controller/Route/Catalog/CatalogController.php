<?php


namespace App\Controller\Route\Catalog;


use App\Controller\Route\Language\LanguageController;
use App\Controller\Services\Catalog\CatalogService;
use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CatalogController extends AbstractController
{
    /**
     * @param TranslatorInterface $translator
     * @param Kernel $kernel
     * @return Response
     */
    public function list(TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = LanguageController::LANGUAGE_CODE_ES;
        $translator->setLocale($lang);

        return $this->render (
            'list/catalog-list.html.twig',
            $this->mapCatalog($lang, $kernel)
        );
    }

    /**
     * @param TranslatorInterface $translator
     * @param Kernel $kernel
     * @return Response
     */
    public function test(TranslatorInterface $translator, Kernel $kernel): Response
    {
        $_ENV['APP_ENV'] = 'dev';
        $lang = LanguageController::LANGUAGE_CODE_ES;
        $translator->setLocale($lang);

        return $this->render (
            'list/catalog-list.html.twig',
            $this->mapCatalog($lang, $kernel)
        );
    }

    /**
     * @param string $lang
     * @param Kernel $kernel
     * @return array
     */
    private function mapCatalog(string $lang, Kernel $kernel)
    {
    return array(
            'sections' => CatalogService::getSections($lang, $kernel, $this->getDoctrine()),
            'catalog' => CatalogService::getItems($lang, $kernel, $this->getDoctrine()),
            'index_url' => $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL)
        );
    }
}