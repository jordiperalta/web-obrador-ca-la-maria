<?php


namespace App\Controller\Route\ContentAPI;


use App\Controller\Route\Language\LanguageController;
use App\Controller\Route\RenderMapTrait;
use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class CatalogController extends AbstractController
{
    use RenderMapTrait;

    /**
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param Kernel $kernel
     * @return Response
     */
    public function productList(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render (
            'main/catalog-content.html.twig',
            $this->getRenderMap($lang, $request, $kernel),
        );
    }

    /**
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param Kernel $kernel
     * @return Response
     */
    public function productModal(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render (
            'main/modal/catalog-content.html.twig',
            $this->getRenderMap($lang, $request, $kernel),
        );
    }

}