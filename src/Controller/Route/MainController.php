<?php

namespace App\Controller\Route;

use App\Controller\Route\Language\LanguageController;
use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MainController extends AbstractController
{
    use RenderMapTrait;

    /**
     * @var string
     */
    protected string $language;

    /**
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param Kernel $kernel
     * @return Response
     */
    public function index(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render(
            'main/index.html.twig',
            array_merge(
                $this->getRenderMap($lang, $request, $kernel),
                [
                    'index_url' => $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL),
                    'delivery_days' => $this->listDeliveryDays($translator),
                ]
            )
        );
    }

    /**
     * @param Request $request
     * @param TranslatorInterface $translator
     * @param Kernel $kernel
     * @return Response
     */
    public function catalog(Request $request, TranslatorInterface $translator, Kernel $kernel): Response
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $translator->setLocale($lang);

        return $this->render(
            'main/catalog.html.twig',
            array_merge(
                $this->getRenderMap($lang, $request, $kernel),
                [
                    'index_url' => $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL),
                    'delivery_days' => $this->listDeliveryDays($translator),
                ]
            )
        );
    }
}
