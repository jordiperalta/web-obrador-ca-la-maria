<?php


namespace App\Controller\Route\Email;


use App\Controller\Route\Language\LanguageController;
use App\Controller\Route\RenderMapTrait;
use App\Controller\Services\Recaptcha\RecaptchaManagerService;
use App\Controller\Services\SiteInfo\SiteInfoService;
use App\Kernel;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Uid\Uuid;
use Symfony\Contracts\Translation\TranslatorInterface;

class MailerController extends AbstractController
{
    use RenderMapTrait;

    /**
     * @var string
     */
    private string $uuid;

    /**
     * @param MailerInterface $mailer
     * @param TranslatorInterface $translator
     * @param Request $request
     * @param Kernel $kernel
     * @return Response
     * @throws TransportExceptionInterface
     * @throws GuzzleException
     */
    public function email(MailerInterface $mailer, TranslatorInterface $translator, Request $request, Kernel $kernel): Response
    {
        $this->validateRecaptcha($request);

        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $email = SiteInfoService::getData($lang, $kernel, $this->getDoctrine())['admin_email'];
        $post = $request->request->all();
        $this->uuid = Uuid::v1();

        $translator->setLocale($lang);

        $this
            ->sendAdminMail($post, $email, $mailer)
            ->sendUserMail($post, $lang, $mailer);

        return $this->render(
            'mailer/contact.html.twig',
            array_merge(
                $this->getRenderMap($lang, $request, $kernel),
                [
                    'index_url' => $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL),
                    'post' => $post
                ]
            )
        );
    }

    /**
     * @param Request $request
     * @return object
     * @throws GuzzleException
     */
    private function validateRecaptcha(Request $request): object
    {
        $validation = RecaptchaManagerService::instance($request)->isValidSubmit();

        if (!$validation->success)
            throw new UnauthorizedHttpException('Invalid token', 'Unauthorized', null, 401);

        return $validation;
    }

    /**
     * @param array $post
     * @param string $to
     * @param MailerInterface $mailer
     * @return MailerController
     * @throws TransportExceptionInterface
     */
    private function sendAdminMail(array $post, string $to, MailerInterface $mailer)
    {
        $email = (new TemplatedEmail())
            ->from(new Address('no-reply@obrador-ca-la-maria.com', 'Obrador Ca La Maria'))
            ->replyTo(new Address($post['email'], $this->composeName($post)))
            ->to($to)
            ->subject('Consulta web Ca La María: ' . $this->uuid)
            ->htmlTemplate('emails/contact-msg-to-admin.html.twig')
            ->context([
                'post' => $post,
                'uuid' => $this->uuid ]);
        $mailer->send($email);

        return $this;
    }

    /**
     * @param array $post
     * @param string $lang
     * @param MailerInterface $mailer
     * @return MailerController
     * @throws TransportExceptionInterface
     */
    private function sendUserMail(array $post, string $lang, MailerInterface $mailer)
    {
        $email = (new TemplatedEmail())
            ->from(new Address('no-reply@obrador-ca-la-maria.com', 'Obrador Ca La Maria'))
            ->to(new Address($post['email'], $this->composeName($post)))
            ->subject('Consulta web Ca La María: ' . $this->uuid)
            ->htmlTemplate('emails/contact-msg-to-user.html.twig')
            ->context([
                'post' => $post,
                'lang' => $lang,
                'uuid' => $this->uuid ]);
        $mailer->send($email);

        return $this;
    }

    /**
     * @param array $post
     * @return string
     */
    private function composeName(array $post)
    {
        return ucfirst($post['fname']) . ' ' . ucfirst($post['lname']);
    }
}