<?php


namespace App\Controller\Route;


use App\Controller\Route\Checkout\CheckoutTrait;
use App\Controller\Services\Catalog\CatalogService;
use App\Controller\Services\PostalCode\PostalCodeService;
use App\Controller\Services\SiteInfo\SiteInfoService;
use App\Kernel;
use App\Model\Bridge\Messages\MessagesBridge;
use App\Model\Bridge\Recaptcha\RecaptchaBridge;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

trait RenderMapTrait
{
    use CheckoutTrait;

    /**
     * @param string $lang
     * @param Request $request
     * @param Kernel $kernel
     * @return array
     */
    public function getRenderMap(string $lang, Request $request, Kernel $kernel): array
    {
        $doctrine = method_exists($this, 'getDoctrine') ? $this->getDoctrine() : null;
        return array(
            'sections' => CatalogService::getSections($lang, $kernel, $doctrine),
            'catalog' => CatalogService::getItems($lang, $kernel, $doctrine),
            'info' => SiteInfoService::getData($lang, $kernel, $doctrine),
            'zones' => PostalCodeService::getData($lang, $kernel, $doctrine),
            'messages' => MessagesBridge::all($kernel),
            'g_recaptcha_siteKey' => (new RecaptchaBridge($request))->getSiteKey(),
        );
    }

    /**
     * @param TranslatorInterface $translator
     * @return string
     */
    public function listDeliveryDays(TranslatorInterface $translator): string
    {
        $days = array_map(function ($item) use ($translator) {return $translator->trans("mult$item");}, $this->getDeliveryDays());
        $last = array_pop($days);

        if (sizeof($days) === 0)
            return $last;

        return implode(", ", $days) . ' ' . $translator->trans('and') . ' ' . $last;
    }
}