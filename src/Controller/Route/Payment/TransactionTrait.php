<?php


namespace App\Controller\Route\Payment;


use App\Controller\Services\SiteInfo\SiteInfoService;
use App\Kernel;
use Doctrine\Persistence\ManagerRegistry;

trait TransactionTrait
{
    /**
     * @param string $response
     * @return bool
     */
    private static function redsysPaymentSuccessful(string $response): bool
    {
        return substr($response, 0, 2) === '00';
    }

    /**
     * @param Kernel $kernel
     * @param ManagerRegistry $doctrine
     * @param string|null $lang
     * @return bool
     */
    private function paymentGatewayOnProductionMode(Kernel $kernel, ManagerRegistry $doctrine, ?string $lang = 'en'): bool
    {
        return in_array(SiteInfoService::getData($lang, $kernel, $doctrine)['gateway_env'], ['PROD', 'PRODUCTION']);
    }
}