<?php


namespace App\Controller\Route\Payment;


use App\Controller\Route\Checkout\CheckoutTrait;
use App\Controller\Route\Language\LanguageController;
use App\Controller\Route\RenderMapTrait;
use App\Entity\Order;
use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConfirmationController extends AbstractController
{
    use RenderMapTrait;
    use CheckoutTrait;

    /**
     * @var Order
     */
    private ?Order $order;

    /**
     * @param string $id
     * @param Request $request
     * @param Kernel $kernel
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function index(string $id, Request $request, Kernel $kernel, TranslatorInterface $translator): Response
    {
        $repository = $this->getDoctrine()->getRepository(Order::class);
        $this->order = $repository->findOneBy(['uuid' => $id]);

        if (!$this->order)
            throw new NotFoundHttpException('Order not found', null, 404);

        $data = $this->getRenderData($request, $kernel, $translator);

        $request->getSession()->remove('processed_order');

        return $this->render('payment/confirmation.html.twig', $data);
    }

    /**
     * @param Request $request
     * @param Kernel $kernel
     * @param TranslatorInterface $translator
     * @return array
     */
    private function getRenderData(Request $request, Kernel $kernel, TranslatorInterface $translator): array
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $misc = $this->getRenderMap($lang, $request, $kernel);
        $translator->setLocale($lang);

        return array_merge(
            $misc,
            [
                'index_url' => $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL),
                'cart' => $this->order->getBasket(),
                'status' => $this->order->getStatus(),
                'test' => $this->order->getType(),
                'delivery' => empty($this->order->getShippingAddress()) ? 'pick' : 'shipment',
                'date' => self::getDeliveryDateMap(strtotime($this->order->getDeliveryDate())),
                'redsys' => [
                    'uuid' => $this->order->getUuid(),
                    'order_id' => $this->order->getOrderId(),
                    'payment_datetime' => $this->order->getPaymentDatetime(),
                    'merchant_reference' => $this->order->getMerchantReference(),
                    'authorization_number' => $this->order->getAuthorizationNumber(),
                    'redsys_response_code' => $this->order->getRedsysResponseCode(),
                    'amount' => $this->order->getAmount()/100,
                 ]
            ]
        );
    }
}