<?php


namespace App\Controller\Route\Payment;


use App\Controller\Route\Checkout\AbstractCheckout;
use App\Controller\Route\Checkout\CheckoutTrait;
use App\Controller\Route\RenderMapTrait;
use App\Controller\Services\Payment\ProcessService;
use App\Kernel;
use App\Model\API\Redsys\RedsysApiClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;

class ReturnController extends AbstractController
{
    use RenderMapTrait;
    use CheckoutTrait;
    use TransactionTrait;

    /**
     * @var RedsysApiClass
     */
    private RedsysApiClass $redsys;

    /**
     * @var string[]
     */
    private static array $orderSessionKeyList = [
        AbstractCheckout::INVOICE_FIELD_INDEX,
        AbstractCheckout::DELIVERY_FIELD_INDEX,
        AbstractCheckout::ORDER_FIELD_INDEX,
        AbstractCheckout::ORDER_ID_FIELD_INDEX,
        AbstractCheckout::UUID_FIELD_INDEX,
        AbstractCheckout::CART_FIELD_INDEX,
    ];

    /**
     * @var object
     */
    private object $data;

    /**
     * @param Request $request
     * @param MailerInterface $mailer
     * @param Kernel $kernel
     * @return Response
     */
    public function index(Request $request, MailerInterface $mailer, Kernel $kernel): Response
    {
        if (!$request->query->all())
            return $this->redirectToRoute('checkout');

        $process = ProcessService::instance(
            $request,
            $this->getDoctrine(),
            $kernel);

        if ($process->verifyRequestErrors())
            return $this->redirectToRoute('checkout');

        if (!$process->isTransactionAlreadyPersisted())
            $process->persistRedsysTransactionAttempt();

        if (!$process->isAcceptedPayment())
            return $this->redirectToRoute('transaction', ['id' => $process->getData()->Ds_Order]);

        $order = $process->confirmationAction($mailer, $kernel, $this->getDoctrine());
        $this->resetOrderSession($request, self::$orderSessionKeyList);

        return $this->redirectToRoute('confirmation', ['id' => $order->getUuid()]);
    }
}