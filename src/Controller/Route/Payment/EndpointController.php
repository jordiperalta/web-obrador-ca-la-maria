<?php


namespace App\Controller\Route\Payment;


use App\Controller\Route\Checkout\CheckoutTrait;
use App\Controller\Route\RenderMapTrait;
use App\Controller\Services\Logs\RedsysLogService;
use App\Controller\Services\Payment\ProcessService;
use App\Kernel;
use App\Model\API\Redsys\RedsysApiClass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;

class EndpointController extends AbstractController
{
    use RenderMapTrait;
    use CheckoutTrait;
    use TransactionTrait;

    /**
     * @var RedsysApiClass
     */
    private RedsysApiClass $redsys;

    /**
     * @var object
     */
    private object $data;

    /**
     * @param Request $request
     * @param MailerInterface $mailer
     * @param Kernel $kernel
     * @return Response
     */
    public function index(Request $request, MailerInterface $mailer, Kernel $kernel): Response
    {
        RedsysLogService::instance($this->getDoctrine())->log($request);

        $process = ProcessService::instance(
            $request,
            $this->getDoctrine(),
            $kernel
        );

        $error = $process->verifyRequestErrors();
        if ($error) {
            return new JsonResponse(['msg' => '400 Bad request: ' . $error], Response::HTTP_BAD_REQUEST);
        }

        if (!$process->isTransactionAlreadyPersisted())
            $process->persistRedsysTransactionAttempt();

        if ($process->isAcceptedPayment())
            $process->confirmationAction($mailer, $kernel, $this->getDoctrine());

        return new JsonResponse(['msg' => '200 OK: Transaction received'], Response::HTTP_OK);
    }
}