<?php


namespace App\Controller\Route\Payment;


use App\Controller\Route\Language\LanguageController;
use App\Controller\Route\RenderMapTrait;
use App\Entity\RedsysTransaction;
use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class TransactionController extends AbstractController
{
    use RenderMapTrait;
    use TransactionTrait;

    /**
     * @param string $id
     * @param Request $request
     * @param Kernel $kernel
     * @param TranslatorInterface $translator
     * @return Response
     */
    public function index(string $id, Request $request, Kernel $kernel, TranslatorInterface $translator): Response
    {
        $transaction = $this->getDoctrine()
            ->getRepository(RedsysTransaction::class)
            ->findOneBy(['order_id' => $id]);

        if (!$transaction)
            throw new NotFoundHttpException('Order not found', null, 404);

        $data = $this->getRenderData($request, $kernel, $translator, $transaction);

        return $this->render('payment/transaction.html.twig', $data);
    }

    /**
     * @param Request $request
     * @param Kernel $kernel
     * @param TranslatorInterface $translator
     * @param RedsysTransaction $transaction
     * @return array
     */
    private function getRenderData(Request $request, Kernel $kernel, TranslatorInterface $translator, RedsysTransaction $transaction): array
    {
        $lang = (new LanguageController)->instance(null, $request, $translator)->getValue();
        $misc = $this->getRenderMap($lang, $request, $kernel);
        $translator->setLocale($lang);

        $status = self::redsysPaymentSuccessful($transaction->getResponseCode());

        return array_merge(
            $misc,
            [
                'index_url' => $this->generateUrl('index',[],UrlGeneratorInterface::ABSOLUTE_URL),
                'redsys' => [
                    'status' => $status ? 'pago aceptado' : 'pago no aceptado',
                    'Ds_Order' => $transaction->getOrderId(),
                    'Ds_Datetime' => $transaction->getDatetime(),
                    'Ds_MerchantCode' => $transaction->getMerchantReference(),
                    'Ds_Response' => $transaction->getResponseCode() ?? '-',
                    'Ds_Amount' => $status ? $transaction->getAmount() : '-'
                ],
            ]
        );
    }
}