{% extends 'admin/admin.html.twig' %}

{% block stylesheets %}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/glyphicons.css') }}">
{% endblock %}

{% block body %}
    <div class="main-admin-body container">
        <h4 class="mb-3">{{ h2title }} <a href="{{ asset('/confirmation') }}/{{ order.getUuid }}" target="_blank">{{ order.getUuid }}</a> </h4>
        <div class="row">
            <div class="col-6">
                <p>
                    <span class="text-size-sm">ESTADO DEL PEDIDO : </span> &nbsp; <strong>{{ order.getStatus }}</strong> &nbsp;
                    {% if order.getType == 'TEST' %}
                        <span class="badge badge-warning">{{ order.getType }}</span>
                    {% elseif order.getType == 'REAL' %}
                        <span class="badge badge-success">{{ order.getType }}</span>
                    {% endif %}
                    <br>
                    <span class="text-size-sm">REFERENCIA TRANSACCIÓN REDSÝS : </span> &nbsp; <strong>{{ order.getOrderId }}</strong><br>
                    <span class="text-size-sm">FECHA TRANSACCIÓN PAGO : </span> &nbsp; <strong>{{ order.getPaymentDatetime }}</strong><br>
                    <span class="text-size-sm">IMPORTE : </span> &nbsp; <strong>{{ (order.getAmount/100)|number_format(2, '.') }} €</strong><br>
                    <span class="text-size-sm">NÚMERO AUTORIZACIÓN REDSÝS : </span> &nbsp; <strong>{{ order.getAuthorizationNumber }}</strong><br>
                    <span class="text-size-sm">CÓDIGO RESPUESTA REDSÝS : </span> &nbsp;
                    <strong><a href="https://pagosonline.redsys.es/codigosRespuesta.html" target="_blank">{{ order.getRedsysResponseCode }}</a></strong><br>
                </p>
                <h5>Detalles del pedido</h5>
                <p>
                    <span class="text-size-sm">TIPO DE PEDIDO :</span> &nbsp;
                    <strong>{{ order.getShippingAddress|length > 0 ? 'ENVÍO A DOMICILIO' : 'RECOGIDA EN TIENDA' }}</strong><br>
                    <span class="text-size-sm">FECHA ENTREGA : </span> &nbsp;
                    <strong>
                        {{ date.weekday|trans|capitalize }},
                        {{ date.day }}
                        {{ date.month|trans }}
                        {{ date.year }}
                    </strong><br>
                </p>
            </div>
            <div class="col-6">
                <h5>Datos del cliente</h5>
                <p>
                    <span class="text-size-sm">NOMBRE : </span> &nbsp;
                    <strong>
                        {{ order.getPersonalInfo['fname'] }} {{ order.getPersonalInfo['lname'] }}
                    </strong><br>
                    <span class="text-size-sm">EMAIL : </span> &nbsp; <strong>{{ order.getPersonalInfo['email'] }}</strong><br>
                    <span class="text-size-sm">TELÉFONO : </span> &nbsp; <strong>{{ order.getPersonalInfo['phone'] }}</strong><br>
                </p>
                {% if order.getShippingAddress|length > 0 %}
                    <h5>Información sobre el envío</h5>
                    <p>
                        <span class="text-size-sm">DIRECCIÓN : </span> &nbsp; <strong>{{ order.getShippingAddress['address'] }}</strong><br>
                        <span class="text-size-sm">POBLACIÓN : </span> &nbsp; <strong>{{ order.getShippingAddress['city'] }}</strong><br>
                        <span class="text-size-sm">CÓDIGO POSTAL : </span> &nbsp; <strong>{{ order.getShippingAddress['zip'] }}</strong><br>
                        <span class="text-size-sm">PROVINCIA : </span> &nbsp; <strong>{{ order.getShippingAddress['zone'] }}</strong><br>
                    </p>
                {% endif %}
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-sm">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" class="table-cell-75">#</th>
                            <th scope="col">Producto</th>
                            <th scope="col" class="table-cell-50 text-center">Precio</th>
                            <th scope="col" class="table-cell-50 text-center">Cantidad</th>
                            <th scope="col" class="text-right">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        {% for key, item in order.getBasket.items %}
                        <tr>
                            <th>{{ key }}</th>
                            <td>{{ item.name|capitalize }}</td>
                            <td class="text-center">{{ item.price|number_format(2, '.') }} €</td>
                            <td class="text-center">x {{ item.qty }}</td>
                            <th class="text-right">{{ item.amount|number_format(2, '.') }} €</th>
                        </tr>
                        {% endfor %}
                        {% if order.getShippingAddress|length > 0 %}
                        <tr>
                            <td> </td>
                            <td class="pt-4">Envío a domicilio</td>
                            <td colspan="2"></td>
                            <td class="text-right pt-4">4.95 €</td>
                        </tr>
                        {% endif %}
                        <tr>
                            <td> </td>
                            <td>
                                <strong>TOTAL PEDIDO</strong>
                                <span class="text-size-xs">({{ order.getBasket.count }} artículos)</span>
                            </td>
                            <td colspan="2"></td>
                            <th class="text-right">{{ (order.getAmount/100)|number_format(2, '.') }} €</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row pt-2 pb-5">
            <div class="col text-right">
                <a href="{{ admin }}/order/list?page={{ page ?? 0 }}" class="btn btn-primary">Volver</a>
            </div>
        </div>
    </div>
{% endblock %}

{% block javascripts %}
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

{% endblock %}